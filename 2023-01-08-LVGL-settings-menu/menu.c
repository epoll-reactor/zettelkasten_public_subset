#include "settings.h"
#include <stdlib.h>

#define ARRAY_SIZE(x) (sizeof((x)) / sizeof(*(x)))

typedef struct {
    void(*callback)(lv_event_t *);
    const char *icon;
    const char *name;
} menu_item_t;

static lv_obj_t *g_settings_list;

/* All other callbacks for each menu entry can launch specified
   binary or render LVGL screen. */
static void on_button_click(lv_event_t *e)
{
    lv_event_code_t code = lv_event_get_code(e);
    lv_obj_t *obj = lv_event_get_target(e);
    LV_UNUSED(obj);
    if(code == LV_EVENT_CLICKED) {
        const char *text = lv_list_get_btn_text(g_settings_list, obj);
        /* Example. */
        LV_LOG_USER("Clicked: %s", text);
        if (!strcmp(text, "Shutdown")) {
            exit(0);
        }
    }
}

static void set_menu_entry_style(lv_obj_t *menu)
{
    lv_obj_set_style_text_font(menu, &lv_font_montserrat_40, 0);
    lv_obj_set_style_pad_top(menu, 50, 0);
    lv_obj_set_style_pad_bottom(menu, 50, 0);
}

static void set_menu_separator_style(lv_obj_t *menu)
{
    lv_obj_set_style_text_font(menu, &lv_font_montserrat_40, 0);
    lv_obj_set_style_pad_top(menu, 40, 0);
    lv_obj_set_style_pad_bottom(menu, 10, 0);
}

void install_menu_section(menu_item_t *items, size_t num_items, const char *section_name)
{
    lv_obj_t *btn = NULL;

    btn = lv_list_add_text(g_settings_list, section_name);
    set_menu_separator_style(btn);

    for (size_t i = 0; i < num_items; ++i) {
        btn = lv_list_add_btn(g_settings_list, items[i].icon, items[i].name);
        lv_obj_add_event_cb(btn, items[i].callback, LV_EVENT_CLICKED, NULL);
        set_menu_entry_style(btn);
    }
}

void settings_install(lv_obj_t *parent)
{
    g_settings_list = lv_list_create(parent);
    lv_obj_set_size(g_settings_list, 1070, 1700);
    lv_obj_center(g_settings_list);

    lv_obj_t *btn;

    menu_item_t files_items[] = {
        { on_button_click, LV_SYMBOL_FILE, "New" },
        { on_button_click, LV_SYMBOL_DIRECTORY, "Open" },
        { on_button_click, LV_SYMBOL_SAVE, "Save" },
        { on_button_click, LV_SYMBOL_CLOSE, "Delete" },
        { on_button_click, LV_SYMBOL_EDIT, "Edit" }
    };
    menu_item_t connectivity_items[] = {
        { on_button_click, LV_SYMBOL_BLUETOOTH, "Bluetooth" },
        { on_button_click, LV_SYMBOL_GPS, "Navigation" },
        { on_button_click, LV_SYMBOL_BATTERY_FULL, "Battery" }
    };
    menu_item_t power_items[] = {
        { on_button_click, LV_SYMBOL_POWER, "Shutdown" },
        { on_button_click, LV_SYMBOL_REFRESH, "Reboot" },
    };

    install_menu_section(files_items, ARRAY_SIZE(files_items), "Files");
    install_menu_section(connectivity_items, ARRAY_SIZE(connectivity_items), "Connectivity");
    install_menu_section(power_items, ARRAY_SIZE(power_items), "Power management");
}
