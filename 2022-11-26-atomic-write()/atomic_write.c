#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <pthread.h>

#define THREADS_NUM 8

void fatal(const char *msg)
{
    perror(msg);
    exit(-1);
}

typedef struct {
    int fd;
} thread_args_t;

void *thread_routine(void *arg)
{
    thread_args_t *args = (thread_args_t *)arg;

    for (int i = 0; i < 100; ++i)
        if (write(args->fd, "Text\n", 5) == -1)
            fatal("write");

    return NULL;
}

int main()
{
    pthread_t        workers[THREADS_NUM];
    int              i;
    int              fd = open("File.txt", O_CREAT | O_RDWR);
    char             read_buf[16384];
    thread_args_t    args = { .fd = fd };

    if (fd == -1)
        fatal("open");

    for (i = 0; i < THREADS_NUM; ++i)
        pthread_create(
            &workers[i],
            /*attrs*/NULL,
            thread_routine,
            &args
        );

    for (i = 0; i < THREADS_NUM; ++i)
        pthread_join(workers[i], NULL);

    if (lseek(fd, 0, SEEK_SET))
        fatal("lseek");

    if (fcntl(fd, F_GETFD) == -1 || errno == EBADF)
        puts("File is not valid");

    if (read(fd, read_buf, sizeof(read_buf)) != 1)
        printf("Buf: %s\n", read_buf);
    else
        fatal("read");

    if (close(fd) == -1)
        fatal("close");
}
