#ifndef __clang__
#	error "Please, run this with clang"
#endif

#include <stdio.h>
#include <sys/time.h>

int without_vectorize(int *a, int *b, int size) {
	printf("Without #pragma's...\n");

	struct timeval stop, start;
	gettimeofday(&start, NULL);

	int mem[size];
	#pragma clang loop vectorize_width(8)
	for (int i = 0; i < size; ++i) {
		mem[i] = i;
		mem[i] += size;
	}

	gettimeofday(&stop, NULL);
	printf("Finished in %lu milliseconds\n\n", (stop.tv_sec - start.tv_sec) * 1000000 + stop.tv_usec - start.tv_usec);

	return mem[0] = mem[sizeof(mem) / sizeof(*mem) - 1];
}

int with_vectorize_width_2(int *a, int *b, int size) {
	printf("With #pragma clang loop vectorize_width(2)...\n");

	struct timeval stop, start;
	gettimeofday(&start, NULL);

	int mem[size];
	#pragma clang loop vectorize_width(2)
	for (int i = 0; i < size; ++i) {
		mem[i] = i;
		mem[i] += size;
	}

	gettimeofday(&stop, NULL);
	printf("Finished in %lu milliseconds\n\n", (stop.tv_sec - start.tv_sec) * 1000000 + stop.tv_usec - start.tv_usec);

	return mem[0] = mem[sizeof(mem) / sizeof(*mem) - 1];
}

int with_vectorize_width_4(int *a, int *b, int size) {
	printf("With #pragma clang loop vectorize_width(4)...\n");

	struct timeval stop, start;
	gettimeofday(&start, NULL);

	int mem[size];
	#pragma clang loop vectorize_width(4)
	for (int i = 0; i < size; ++i) {
		mem[i] = i;
		mem[i] += size;
	}

	gettimeofday(&stop, NULL);
	printf("Finished in %lu milliseconds\n\n", (stop.tv_sec - start.tv_sec) * 1000000 + stop.tv_usec - start.tv_usec);

	return mem[0] = mem[sizeof(mem) / sizeof(*mem) - 1];
}

int with_vectorize_width_8(int *a, int *b, int size) {
	printf("With #pragma clang loop vectorize_width(8)...\n");

	struct timeval stop, start;
	gettimeofday(&start, NULL);

	int mem[size];
	#pragma clang loop vectorize_width(8)
	for (int i = 0; i < size; ++i) {
		mem[i] = i;
		mem[i] += size;
	}

	gettimeofday(&stop, NULL);
	printf("Finished in %lu milliseconds\n\n", (stop.tv_sec - start.tv_sec) * 1000000 + stop.tv_usec - start.tv_usec);

	return mem[0] = mem[sizeof(mem) / sizeof(*mem) - 1];
}

int with_just_vectorize_enable(int *a, int *b, int size) {
	printf("With #pragma clang loop vectorize(enable)...\n");

	struct timeval stop, start;
	gettimeofday(&start, NULL);

	int mem[size];
	#pragma clang loop vectorize(enable)
	for (int i = 0; i < size; ++i) {
		mem[i] = i;
		mem[i] += 123;
	}

	gettimeofday(&stop, NULL);
	printf("Finished in %lu milliseconds\n\n", (stop.tv_sec - start.tv_sec) * 1000000 + stop.tv_usec - start.tv_usec);

	return mem[0] = mem[sizeof(mem) / sizeof(*mem) - 1];
}

int main() {
	#define SIZE 100000

	int a[SIZE];
	int b[SIZE];

	without_vectorize         (&*a, &*b, SIZE);
	with_vectorize_width_2    (&*a, &*b, SIZE);
	with_vectorize_width_4    (&*a, &*b, SIZE);
	with_vectorize_width_8    (&*a, &*b, SIZE);
	with_just_vectorize_enable(&*a, &*b, SIZE);
}
