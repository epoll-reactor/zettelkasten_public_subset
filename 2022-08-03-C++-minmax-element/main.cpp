#include <algorithm>
#include <iostream>

int main() {
	int array[] = { 9, 4, 2, 1, 0, 5, 7, 3 };
	auto [min, max] =
		std::minmax_element(
			std::begin(array),
			std::  end(array));
	std::cout << "{ 9, 4, 2, 1, 0, 5, 7, 3 }\n";
	std::cout << "min: " << *min << '\n';
	std::cout << "max: " << *max << '\n';
}
