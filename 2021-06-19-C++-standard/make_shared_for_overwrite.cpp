#include <memory>
#include <chrono>
#include <thread>
#include <algorithm>
#include <iostream>
#include <iomanip>

class scoped_timer {
	std::chrono::time_point<std::chrono::system_clock> start =
		std::chrono::system_clock::now();

public:
	~scoped_timer() {
		auto elapsed = std::chrono::system_clock::now() - start;
		std::cout << "elapsed " << std::chrono::duration_cast<std::chrono::milliseconds>(elapsed)
			.count() << " ms." << std::endl;
	}
};

int main() {
	{
		[[maybe_unused]] scoped_timer _;
		std::cout << "for make_shared...               ";
		std::make_shared<int[666][666][666]>();
	}
	{
		[[maybe_unused]] scoped_timer _;
		std::cout << "for make_shared_for_overwrite... ";
		std::make_shared_for_overwrite<int[666][666][666]>();
	}
}
