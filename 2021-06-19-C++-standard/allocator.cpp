#include <memory>
#include <iostream>

// Without rebind_alloc we cannot use given allocator type
// in such context, so we need to obtain the same allocator,
// but for another type.
template <typename t, typename allocator = std::allocator<t>>
class rebind_allocator_usage {
  struct internal_data_repr {
    t value;
    bool some_flag;
  };

  using external_allocator = allocator;
  using internal_allocator =
    typename std::allocator_traits<external_allocator>::template rebind_alloc<internal_data_repr>;

  using external_traits = std::allocator_traits<external_allocator>;
  using internal_traits = std::allocator_traits<internal_allocator>;

public:
  rebind_allocator_usage() {
    t *value                     = external_traits::allocate(external_allocator_, 1);
    internal_data_repr *internal = internal_traits::allocate(internal_allocator_, 1);

    external_traits::construct(external_allocator_, value, t{});
    internal_traits::construct(internal_allocator_, internal,
      internal_data_repr{t{}, false});

    external_traits::destroy(external_allocator_, value);
    internal_traits::destroy(internal_allocator_, internal);
    external_traits::deallocate(external_allocator_, value, 1);
    internal_traits::deallocate(internal_allocator_, internal, 1);
  }

private:
  external_allocator external_allocator_;
  internal_allocator internal_allocator_;
};

int main() {
  {
    // Conversion.
    std::allocator<int> integer_allocator;
    std::allocator<char> character_allocator = integer_allocator;
  }
  {
    // Idk, this is always true.
    static_assert(
      std::is_same_v<
        std::allocator<int>::propagate_on_container_move_assignment,
        std::true_type
      >
    );
  }
  {
    if constexpr (
      std::allocator<int>::propagate_on_container_move_assignment::value
    ) {
      std::allocator<int> custom_allocator =
        std::allocator_traits<std::allocator<int>>
          ::select_on_container_copy_construction(
            std::allocator<int>{}
          );
    }
  }
  {
    static_assert(
      std::uses_allocator_v<std::string, std::allocator<char>>
    );
    // This is always true probably for all types being template
    // parameter of std::allocator...
    static_assert(
      std::uses_allocator_v<std::string, std::allocator<void *>>
    );
  }
  {
    std::cout << "std::allocator can allocate up to " <<
      std::allocator_traits<
        std::allocator<int>
      >::max_size(std::allocator<int>{}) << " (" <<
      "size_t(-1) / sizeof(int)) bytes" << std::endl;
  }
  {
    static_assert(
      std::is_same_v<
        std::allocator<char>,
        std::allocator_traits<std::allocator<int>>::rebind_alloc<char>
      >
    );
  }
  {
    [[maybe_unused]]
    rebind_allocator_usage<int> usage;
  }
}
