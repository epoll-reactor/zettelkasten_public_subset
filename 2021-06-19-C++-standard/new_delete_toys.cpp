#include <memory>
#include <new>
#include <iostream>

// All methods are implicitly static. Doesn't matter, if
// we mark them as static by hand.
struct new_delete_user {
	static void *operator new(size_t size) {
		std::cout << __PRETTY_FUNCTION__ << std::endl;
		return malloc(size);
	}
	void *operator new(size_t size, std::align_val_t) {
		std::cout << __PRETTY_FUNCTION__ << std::endl;
		return malloc(size);
	}
	void *operator new(size_t size, std::nothrow_t) {
		std::cout << __PRETTY_FUNCTION__ << std::endl;
		return malloc(size);
	}
	void *operator new(size_t size, std::string &&) {
		std::cout << __PRETTY_FUNCTION__ << std::endl;
		return malloc(size);
	}
	void operator delete(void *memory) {
		std::cout << __PRETTY_FUNCTION__ << std::endl;
		free(memory);
	}
};

struct forbid_new_delete {
	void *operator new(size_t) = delete;
//	void *operator new(size_t, std::align_val_t) = delete;
//	void *operator new(size_t, const std::nothrow_t &) = delete;
	void operator delete(void *) = delete;
};

int main() {
	delete new new_delete_user;
	// For some reason, we can't use other than new(size_t)
	// overloads with deleted only this one. Also, we can omit this
	// limit with ::operator new.

	// new forbid_new_delete;
	// new (std::nothrow) forbid_new_delete;
	void *mem = ::operator new(sizeof(forbid_new_delete));
	forbid_new_delete *ptr = static_cast<forbid_new_delete *>(mem);

	new_delete_user::operator new(0);
	new_delete_user::operator new(0, std::align_val_t(256));
	new_delete_user::operator new(0, std::nothrow);
	new_delete_user::operator new(0, "Hello world haha");

	new (std::align_val_t(256)) new_delete_user;
	new (std::nothrow) new_delete_user;
	new ("holey language") new_delete_user;

	std::make_shared<new_delete_user>();
	// Hmm...
	std::make_shared<forbid_new_delete>();
}
