#include <math.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <gsl/gsl_randist.h>

float lerp(float a, float b, float f)
  {
    return a + f * (b - a);
  }

void printf_n(char c, uint32_t count)
  {
    for (uint32_t i = 0; i < count; ++i)
      {
        putchar (c);
      }

    fflush (stdout);
  }

void draw_bars (const char *distribution_name, float *mem, size_t mem_siz, bool draw_min_max)
  {
    printf("\t%s\n\n", distribution_name);
  
    static const size_t height = 10;
    static const size_t bar_width = 4;
    static const size_t pad = 1;
    static const size_t off = 0;

    float min = mem[0], max = 0;

    for (size_t i = 0; i < mem_siz; ++i)
      {
        if (mem[i] > max)
          {
            max = mem[i];
          }
        else if (mem[i] < min)
          {
            min = mem[i];
          }
      }

    div_t *qr = (div_t *) calloc (mem_siz, sizeof (div_t));
    if (!qr)
      {
        perror ("calloc()");
        exit (-1);
      }

    for (size_t i = 0; i < mem_siz; ++i)
      {
        qr[i] = div (lerp (0.0, height * 8, (mem[i] - min) / (max - min) ), 8);
      }

    for (size_t h = height; h-- > 0; )
      {
        printf_n (' ', off);

        for (size_t qr_i = 0; qr_i < mem_siz; ++qr_i)
          {
            int quot = qr[qr_i].quot;
            int rem  = qr[qr_i].rem;

            uint8_t d[] = { 0xe2, 0x96, 0x88 }; // Full Block: '█'
            if (quot < (int) h)
              {
                d[0] = ' ';
                d[1] = '\0';
              }
            else
              {
                if (quot == (int) h)
                  {
                    d[2] -= (7 - rem);
                  }
              }

            for (size_t i = 0; i < bar_width; ++i)
              {
                printf ("%s", d);
              }

            printf_n (' ', pad);
          }

        if (draw_min_max && height > 1)
          {
            if (height - 1 == h)
              {
                printf ("┬ %f", max);
              }
            else
              {
                if (h)
                  {
                    printf ("│ ");
                  }
                else
                  {
                    printf ("┴  %f", min);
                  }
              }
          }
        putchar('\n');
      }

    free(qr);
  }

typedef struct
  {
    const char *name;
    double (*generator) (const gsl_rng *, double);
    double param;

  } distribution_data_t;

static distribution_data_t tests[] = {
  { "Gaussian distribution", gsl_ran_gaussian_ratio_method, 4.0 },
  { "Exponential distribution", gsl_ran_exponential, 2 },
  { "Chi-squared distribution (#1)", gsl_ran_chisq, 1 },
  { "Chi-squared distribution (#2)", gsl_ran_chisq, 2 },
  { "Chi-squared distribution (#3)", gsl_ran_chisq, 3 },
};

void test (distribution_data_t *test_data)
  {
    const int norm = 10000;

    int pairs [10000] = {0};

    gsl_rng *rng = gsl_rng_alloc (gsl_rng_default);

    for (int n = 0; n != norm; ++n)
      {
        /** Offset is to show gaussian-like distributions correctly. */
        ++pairs [ (int) round ( test_data->generator (rng, test_data->param) ) + 10 ];
      }

    float bars [1024] = {0};
    static int bars_inserted = 0;

    for (int n = 0; n != norm; ++n)
      {
        if (pairs[n] != 0)
          {
            bars [bars_inserted++] = pairs[n] * (1.0f / norm);
          }
      }

    draw_bars (test_data->name,
               bars,
               bars_inserted,
               /*draw_min_max=*/true);

    gsl_rng_free (rng);
    bars_inserted = 0;

    putchar ('\n');
  }

int main ()
  { 
    static int tests_size = sizeof (tests) / sizeof (*tests);

    for (int i = 0; i < tests_size; ++i)
      {
        test (&tests [i]);
      }
  }
