#include <stdio.h>
#include <libunwind.h>
#include <ucontext.h>
#include <sys/user.h>

void print_stack_trace() {
    unw_cursor_t cursor;
    unw_context_t context;

    // Get the current context
    unw_getcontext(&context);

    // Initialize the cursor to the current frame
    unw_init_local(&cursor, &context);

    // Iterate over the stack frames and print the function names
    while (unw_step(&cursor) > 0) {
        unw_word_t offset, pc;
        char symbol[256];

        // Get the program counter for this frame
        unw_get_reg(&cursor, UNW_REG_IP, &pc);

        // Get the name of the function at this frame's program counter
        if (unw_get_proc_name(&cursor, symbol, sizeof(symbol), &offset) == 0) {
            printf("  [%p] %s+0x%lx\n", (void*)pc, symbol, offset);
        } else {
            printf("  [%p] <unknown function>\n", (void*)pc);
        }
    }
}

void dump_registers(ucontext_t* ucontext) {
    printf("Register dump:\n");
    struct user_regs_struct* regs = (struct user_regs_struct*) &(ucontext->uc_mcontext.gregs);

    printf("  RIP: %llx\n", regs->rip);
    printf("  RSP: %llx\n", regs->rsp);
    printf("  RBP: %llx\n", regs->rbp);
    printf("  RAX: %llx\n", regs->rax);
    printf("  RBX: %llx\n", regs->rbx);
    printf("  RCX: %llx\n", regs->rcx);
    printf("  RDX: %llx\n", regs->rdx);
    printf("  RSI: %llx\n", regs->rsi);
    printf("  RDI: %llx\n", regs->rdi);
    printf("  R8:  %llx\n", regs->r8);
    printf("  R9:  %llx\n", regs->r9);
    printf("  R10: %llx\n", regs->r10);
    printf("  R11: %llx\n", regs->r11);
    printf("  R12: %llx\n", regs->r12);
    printf("  R13: %llx\n", regs->r13);
    printf("  R14: %llx\n", regs->r14);
    printf("  R15: %llx\n", regs->r15);
}

void foo() {
    print_stack_trace();
}

void bar() {
    foo();
}

int main() {
    bar();

    ucontext_t ucontext;
    getcontext(&ucontext); // Get the current context

    dump_registers(&ucontext);

    return 0;
}