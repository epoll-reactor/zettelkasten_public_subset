/// https://clang.llvm.org/docs/RAVFrontendAction.html
#include "clang/AST/ASTConsumer.h"
#include "clang/AST/RecursiveASTVisitor.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Frontend/FrontendAction.h"
#include "clang/Tooling/Tooling.h"
#include <fstream>

const bool ContinueASTTraversal = true;
const bool BreakASTTraversal = false;

class FindNamedClassVisitor
  : public clang::RecursiveASTVisitor<FindNamedClassVisitor> {
public:
  FindNamedClassVisitor(clang::ASTContext *Ctx)
    : ASTCtx(Ctx) {}

  bool VisitCXXRecordDecl(clang::CXXRecordDecl *Decl) {
    if (Decl->getQualifiedNameAsString() == "TestingClangStuff::FindNamedClassVisitor") {
      clang::FullSourceLoc Loc = ASTCtx->getFullLoc(Decl->getBeginLoc());
      if (Loc.isValid())
        llvm::outs() << "Found declaration at "
                     << Loc.getSpellingLineNumber() << ":"
                     << Loc.getSpellingColumnNumber() << "\n";
    }
    return ContinueASTTraversal;
  }

private:
  clang::ASTContext *ASTCtx;
};

class FindNamedClassConsumer : public clang::ASTConsumer {
public:
  explicit FindNamedClassConsumer(clang::ASTContext *Ctx)
    : Visitor(Ctx) {}

  void HandleTranslationUnit(clang::ASTContext &Ctx) override {
    Visitor.TraverseDecl(Ctx.getTranslationUnitDecl());
  }

private:
  FindNamedClassVisitor Visitor;
};

class FindNamedClassAction : public clang::ASTFrontendAction {
public:
  std::unique_ptr<clang::ASTConsumer> CreateASTConsumer(
    clang::CompilerInstance &Compiler, llvm::StringRef /*InFile*/) override {
    return std::make_unique<FindNamedClassConsumer>(&Compiler.getASTContext());
  }
};

int main(int argc, char **argv) {
  if (argc > 1) {
    clang::tooling::runToolOnCode(std::make_unique<FindNamedClassAction>(), argv[1]);
  }
}
