#include <setjmp.h>
#include <stdio.h>

jmp_buf fatal_error_buf;

void handle_fatal_error() {
	printf("Error occurred, cannot parse.");
}

void parse(int arg) {
	printf("For parse `%d`...\n", arg);
	if (arg == 2) {
		longjmp(fatal_error_buf, 1);
		__builtin_trap();
	}
	printf("After\n");
}

int main() {
	if (!setjmp(fatal_error_buf)) {
		parse(1);
		parse(2);
		parse(3);
	} else {
		handle_fatal_error();
	}
}
