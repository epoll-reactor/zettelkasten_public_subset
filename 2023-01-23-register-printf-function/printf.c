#include <printf.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct {
    int line_no;
    int col_no;
}
ast_t;

int printf_ast(FILE *stream,  const struct printf_info *info, const void *const *args)
{
    char *buf = NULL;
    const ast_t *ast = *((const ast_t **) (args[0]));
    int len = asprintf(&buf, "<AST %p line:%d, col:%d>", ast, ast->line_no, ast->col_no);

    if (len == -1)
        return -1;

    len = fprintf(
        stream, "%*s",
        info->width * (info->left ? -1 : 1),
        buf
    );

    free(buf);
    return len;
}

int printf_ast_arginfo(const struct printf_info *info, size_t n, int *argtypes)
{
    (void)info;
    if (n > 0) argtypes[0] = PA_POINTER;
    return 1;
}

int main()
{
    ast_t ast = { 123, 456 };

    register_printf_function('A', printf_ast, printf_ast_arginfo);

    printf("%A\n", &ast);
}
