#include <stdio.h>
#include <string.h>
#include <alloca.h>

void printf_n(int count, char c)
{
	printf("%s\n", (char *)memset(alloca(count), c, count));
}

int main()
{
	for (int i = 1; i <= 32; i *= 2)
		printf_n(i, '*');
}
