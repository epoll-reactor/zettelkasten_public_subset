#include "window.h"
#include "lvgl/lvgl.h"
#include <stdio.h>
#include <math.h>

#define DASH_WIDTH   100
#define DASH_HEIGHT   20
#define BALL_SIZE     40

lv_obj_t *g_player_dash; /// Bottom dash to move to.
lv_obj_t *g_computer_dash; /// Top dash that "plays" with us.
lv_obj_t *g_ball; // Ball, that bouncing from dashes.

static int game_get_center_dash_x_coord(int x)
{
    /// +---------+
    ///      ^    ^
    ///      |    x
    ///      centered x
    return x - DASH_WIDTH / 2;
}

static void game_player_dash_pressed_cb(lv_event_t *event)
{
    lv_point_t dash_coords;
    lv_indev_get_point(lv_indev_get_act(), &dash_coords);
    int dash_x = dash_coords.x;

    if (dash_x < 0 || dash_x > LV_HOR_RES - DASH_WIDTH)
        return;

    lv_obj_t *dash = lv_event_get_target(event);
    lv_obj_set_x(dash, game_get_center_dash_x_coord(dash_x));
}

static void game_install_player_dash()
{
    lv_obj_t **bar = &g_player_dash;
    *bar = lv_bar_create(lv_scr_act());
    lv_obj_set_size(*bar, DASH_WIDTH, DASH_HEIGHT);
    lv_obj_set_y(*bar, LV_VER_RES - 20);
    lv_bar_set_value(*bar, /*"loading"=*/100, LV_ANIM_OFF);
    lv_obj_add_event_cb(*bar, game_player_dash_pressed_cb, LV_EVENT_PRESSING, /*data=*/NULL);
}

static void game_install_computer_dash()
{
    lv_obj_t **bar = &g_computer_dash;
    *bar = lv_bar_create(lv_scr_act());
    lv_obj_set_size(*bar, DASH_WIDTH, DASH_HEIGHT);
    lv_obj_set_y(*bar, 0);
    lv_bar_set_value(*bar, /*"loading"=*/100, LV_ANIM_OFF);
}

static void game_ball_movement_y_cb(void *object, int y)
{
    lv_obj_set_y(object, y);
}

static void game_install_ball()
{
    lv_obj_t **ball = &g_ball;
    *ball = lv_bar_create(lv_scr_act());
    lv_obj_set_size(*ball, BALL_SIZE, BALL_SIZE);
    lv_obj_set_x(*ball, LV_HOR_RES / 2);
    lv_obj_set_y(*ball, LV_VER_RES - 90);
    lv_bar_set_value(*ball, /*"Loading"=*/100, LV_ANIM_OFF);

    lv_anim_t anim;
    lv_anim_init(&anim);
    lv_anim_set_var(&anim, *ball);
    lv_anim_set_values(&anim, DASH_HEIGHT, LV_VER_RES - DASH_HEIGHT * 2);
    lv_anim_set_time(&anim, /*ms=*/2000); /// Top-to-down movement.
    lv_anim_set_playback_time(&anim, /*ms=*/2000); /// Down-to-top movement.
    lv_anim_set_repeat_count(&anim, LV_ANIM_REPEAT_INFINITE);
    lv_anim_set_path_cb(&anim, lv_anim_path_ease_in_out);

    lv_anim_set_exec_cb(&anim, game_ball_movement_y_cb);
    lv_anim_start(&anim);
}

void game_install_window()
{
    game_install_player_dash();
    game_install_computer_dash();
    game_install_ball();
}

/// Increase ball size if it near the center of the screen
/// (Y axis), and decrease if ball is near the both of dashes.
static int dynamic_resized_ball_size()
{
    int y = lv_obj_get_y(g_ball);

    return
        exp(
            log(
                ((y < (LV_VER_RES - DASH_HEIGHT * 2) / 2)
                    ? y
                    : LV_VER_RES - y
                )
            ) / /*closer to 1 -> bigger*/1.3
        );
}
 
/// Move ball over X axis and attach "AI" dash to ball.
/// Therefore, "AI" cannot lose at all.
///
/// \note: Should be called at each game iteration.
static void game_ball_x_movement()
{
    static bool move_left = false;
    static int x = 0;

    lv_obj_set_x(g_ball, x);
    lv_obj_set_x(g_computer_dash, x - DASH_WIDTH / 2);

    if (x >= LV_HOR_RES - dynamic_resized_ball_size()) move_left = true;
    if (x <= 0) move_left = false;

    x += move_left ? -1 : 1;
}

/// Make ball bigger at center and smaller near the
/// edges.
///
/// \note: Should be called at each game iteration.
static void game_ball_dynamic_resize()
{
    int y = lv_obj_get_y(g_ball);
    int new_size = dynamic_resized_ball_size();

    lv_obj_set_size(g_ball, new_size, new_size);
}

static void game_check_if_game_over()
{
    int dash_x = lv_obj_get_x(g_player_dash);
    int dash_y = lv_obj_get_y(g_player_dash);
    int ball_x = lv_obj_get_x(g_ball);
    int ball_y = lv_obj_get_y(g_ball);

    if (ball_y != LV_VER_RES - DASH_HEIGHT * 2)
        return;

    if (ball_x < (dash_x - DASH_WIDTH / 2) ||
        ball_x > (dash_x + DASH_WIDTH / 2)) {
        lv_obj_t *msgbox = lv_msgbox_create(
            /*parent=*/lv_scr_act(),
            /*title=*/"",
            /*txt=*/"You lose",
            /*btn_txts=*/NULL,
            /*add_close_btn=*/false
        );
        lv_obj_center(msgbox);
    }
}

void game_do_event_loop_actions()
{
    game_ball_x_movement();
    game_ball_dynamic_resize();
    game_check_if_game_over();
}
