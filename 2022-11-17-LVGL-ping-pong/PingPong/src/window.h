#ifndef GAME_WINDOW_H_
#define GAME_WINDOW_H_

/// Create and install all window details (dashes, ball).
void game_install_window();
void game_do_event_loop_actions();

#endif // GAME_WINDOW_H_