#include "init.h"
#include "lv_drivers/sdl/sdl.h"
#include "../lv_drv_conf.h"

#define DISPLAY_SIZE SDL_HOR_RES * 100

lv_disp_t *game_init_display()
{
    static lv_disp_draw_buf_t display_buf;
    static lv_color_t color_buf[DISPLAY_SIZE];
    lv_disp_draw_buf_init(&display_buf, /*buf1=*/color_buf, /*buf2=*/NULL, DISPLAY_SIZE);

    static lv_disp_drv_t display_driver;
    lv_disp_drv_init(&display_driver);
    display_driver.draw_buf = &display_buf;
    display_driver.flush_cb = sdl_display_flush;
    display_driver.hor_res = SDL_HOR_RES;
    display_driver.ver_res = SDL_VER_RES;

    return lv_disp_drv_register(&display_driver);
}

void game_init_theme(lv_disp_t *display)
{
    lv_theme_t *theme = lv_theme_default_init(
        display,
        lv_palette_main(LV_PALETTE_BLUE),
        lv_palette_main(LV_PALETTE_RED),
        LV_THEME_DEFAULT_DARK,
        LV_FONT_DEFAULT
    );
    lv_disp_set_theme(display, theme);
}

void game_init_devices()
{
    static lv_indev_drv_t mouse_driver;
    lv_indev_drv_init(&mouse_driver);
    mouse_driver.type = LV_INDEV_TYPE_POINTER;
    mouse_driver.read_cb = sdl_mouse_read;
    lv_indev_t *mouse_indev = lv_indev_drv_register(&mouse_driver);

    LV_IMG_DECLARE(mouse_cursor_icon);
    lv_obj_t *cursor_obj = lv_img_create(lv_scr_act());
    lv_img_set_src(cursor_obj, &mouse_cursor_icon);
    lv_indev_set_cursor(mouse_indev, cursor_obj);
}