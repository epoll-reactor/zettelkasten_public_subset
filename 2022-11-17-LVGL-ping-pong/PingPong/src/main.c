#include <stdlib.h>
#include <unistd.h>
#include <SDL2/SDL.h>
#include "lvgl/lvgl.h"
#include "lv_drivers/sdl/sdl.h"
#include "init.h"
#include "window.h"

static void init_global_context()
{
    sdl_init();
    lv_disp_t *display = game_init_display();
    game_init_theme(display);
    game_init_devices();
}

int main()
{
    lv_init();
    init_global_context();
    game_install_window();

    while(1) {
        lv_timer_handler();
        game_do_event_loop_actions();
        usleep(5 * 250);
    }
}