#ifndef GAME_INIT_H_
#define GAME_INIT_H_

typedef struct _lv_disp_t lv_disp_t;
typedef struct _lv_group_t lv_group_t;

lv_disp_t *game_init_display();
void       game_init_theme(lv_disp_t *display);
void       game_init_devices();

#endif // GAME_INIT_H_