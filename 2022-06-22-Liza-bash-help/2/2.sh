function users_count() {
  echo "Liczba zalogowanych z cyfra w loginie: `who | tr -s ' ' | cut -d ' ' -f 1 | egrep ".*\d.*" | wc -l`"
}

export -f users_count

watch -n 4 users_count
