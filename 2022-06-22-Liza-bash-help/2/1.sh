[[ "$#" -ne 1 ]] && echo "Ожидается 1 аргумент" && exit

times_logged_in=`last | egrep "\b$1\b" | wc -l`
echo "Пользователь $1 заходил $times_logged_in раз"
