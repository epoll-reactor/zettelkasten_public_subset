#include <stddef.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Stolen somewhere from Github. */
typedef struct block_meta {
    int size;
    struct block_meta *next;
    int free;
} block_meta_t;

block_meta_t *__malloc_global_base;
block_meta_t *__malloc_global_last;

block_meta_t *__malloc_request_space(int size)
{
    block_meta_t *block = sbrk(0);
    int request = brk(block + size + sizeof(block_meta_t));
    if (request == -1)
        return NULL;
    if (__malloc_global_last)
        __malloc_global_last->next = block;
    block->size = size;
    block->next = NULL;
    block->free = 0;
    return block;
}

void *malloc(size_t size)
{
    char buf[128];
    sprintf(buf, "Malloc of %zu bytes\n", size);
    write(1, buf, strlen(buf));
    block_meta_t *block;
    if (size == 0)
        return NULL;
    if (!__malloc_global_base) {
        block = __malloc_request_space(size);
        if (!block)
            return NULL;
        __malloc_global_base = block;
    } else {
        block_meta_t *current = __malloc_global_base;
        __malloc_global_last = __malloc_global_base;
        while (current) {
            if (current->free == 1 && current->size >= size)
                return current + 1;
            __malloc_global_last = current;
            current = current->next;
        }
        block = current;
        if (!block) {
            block = __malloc_request_space(size);
            if (!block)
                return NULL;
        } else {
            block->free = 0;
        }
    }
    return block + 1;
}

void free(void *ptr)
{
    if (!ptr) return;
    block_meta_t *block_ptr = ptr - sizeof(block_meta_t);
    if (block_ptr->free == 1)
        abort();
    block_ptr->free = 1;
}

void *realloc(void *ptr, size_t size)
{
    /* Holy fuck */
    free(ptr);
    return malloc(size);
}
