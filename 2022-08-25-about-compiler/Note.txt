Hey, there is short story about my experience in field of compilers develompent.

https://github.com/epoll-reactor/weak_compiler/

It has two branches, so I will describe what is contained inside.

Main branch - Lexical analysis, Syntax (LL(1) recursive parser) analyzing, implementation of Control Flow Graph and SSA form.
So I've implemented SSA with this algo: https://www.cs.utexas.edu/~pingali/CS380C/2010/papers/ssaCytron.pdf,
but there is another, more interesting one: https://www.sciencedirect.com/science/article/pii/S1571066107005324,
which I've didn't have a pleasure to implement yet.

Introduce-llvm branch - The same things that are in main, but without written by hand CFG, and with code generation to LLVM IR. So my SSA algorithm was removed there also, since LLVM take care about it. This language also have binary compatibility with C (thanks LLVM for that). Also it have some type-checking system. And at this stage it can compile given input program to executable, so we can even implement some algorithms in this language.

The next stages of my work are: 100% clearly defined type system (strong or weak, implicit type conversion rules, like in C++); self-written backend (platform-dependent code generation), different graph-based optimization methods.

P.S. I used to get some ideas from Clang source code, since it well documented and written.

Also I am familiar a bit with Z3 theorem prover.

Thanks for your attention!
