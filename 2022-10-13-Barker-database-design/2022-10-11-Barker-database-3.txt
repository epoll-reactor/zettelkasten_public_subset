Zadanie 3
Załóżmy, że chcemy przechować w relacyjnej bazie danych informacje:
• o gromadach zuchowych istniejących w szkole (jednoznaczny identyfikator, nazwa gromady, rok założenia,
opcjonalny opis),
• o uczniach (jednoznaczny identyfikator, imię, nazwisko, data urodzenia, płeć, opcjonalny numer telefonu
kontaktowego, adres zamieszkania),
• o tym, do której gromady zuchowej uczeń należy.
Zakładamy, że każdy uczeń należy do co najwyżej jednej gromady (czyli może nie należeć do żadnej), a gromada musi się
składać z wielu uczniów.
Narysuj diagram logiczny i diagram relacyjny bazy danych, która w sposób poprawny i efektywny będzie przechowywała
żądane informacje. Spraw, aby baza była w trzeciej postaci normalnej.

   /------------\                /--------------------\
   | gromada    |                |       uczen        |
   +------------+                +--------------------|
   | # * id     |                | # * id             |
   |   * nazwa  |-------- - - - <|   * imie           |
   |   * od     |                |   * nazwisko       |
   | o   opis   |                |   * data urodzenia |
   \------------/                |   * plec           |
                                 | o   nr telefonu    |
                                 |   * adres          |
                                 \--------------------/


   +------------+                +--------------------+
   | gromada    |                |       uczen        |
   +------------+                +--------------------|
   | id      PK | 1          inf | id              PK |
   | nazwa   NN |----------------| imie            NN |
   | od      NN |                | nazwisko        NN |
   | opis       |                | data urodzenia  NN |
   +------------+                | plec            NN |
                                 | nr telefonu        |
                                 | adres           NN |
                                 | id-gromady   FK    | < Maybe NULL
                                 +--------------------+
