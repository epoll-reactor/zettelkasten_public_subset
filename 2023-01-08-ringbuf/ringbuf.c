#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>

/// \brief Circular buffer.
typedef struct {
    /// Buffer, elements are stored here.
    void  *memory;
    /// Size of buffer in bytes. User should care about it.
    size_t capacity;
    /// Index of first free memory location in buffer.
    size_t start;
    /// How much bytes are occupied.
    size_t consumed;
} ringbuf_t;

/*!
 * \brief   Initialize buffer.
 * \param   rb         - pointer to buffer.
 * \param   memory     - external block of memory.
 * \param   size_bytes - size of memory.
 * \return  false if memory is NULL, true otherwise.
 */
bool ringbuf_create(ringbuf_t *rb, void *memory, size_t size_bytes);

/*!
 * \brief   Tell, how much bytes are available.
 * \return  Amount of free space.
 */
size_t ringbuf_get_free_space(ringbuf_t *rb);

/*!
 * \brief   Try to insert value to buffer.
 * \param   rb   - pointer to buffer.
 * \param   data - pointer to input.
 * \param   len  - input size in bytes.
 * \return  false if there is no enough memory to insert object.
 */
bool ringbuf_put(ringbuf_t *rb, void *data, size_t len);

/*!
 * \brief   Try to store .
 * \param   rb   - pointer to buffer.
 * \param   data - pointer to output value.
 * \param   len  - input size in bytes.
 * \return  false if there is no enough memory to insert object.
 */
bool ringbuf_get(ringbuf_t *rb, void *data, size_t len);

bool ringbuf_create(ringbuf_t *rb, void *memory, size_t size_bytes)
{
    rb->capacity = size_bytes;
    rb->memory = memory;
    rb->start = 0;
    rb->consumed = 0;

    return rb->memory != NULL;
}

size_t ringbuf_get_free_space(ringbuf_t *rb)
{
    return rb->capacity - rb->consumed;
}

bool ringbuf_put(ringbuf_t *rb, void *data, size_t len)
{
    if (ringbuf_get_free_space(rb) < len) {
        fprintf(
          stderr,
          "Overflow in ringbuf %p! size=%zu, consumed=%zu, len=%zu\n",
          (void *)rb, rb->capacity, rb->consumed, len
        );
        return false;
    }

    size_t first_chunk_len = rb->capacity - rb->start;
    if (first_chunk_len > len) {
        memcpy((uint8_t *)rb->memory + rb->start, data, len);
        rb->start += len;
        rb->consumed += len;
        return true;
    }

    memcpy((uint8_t*)rb->memory + rb->start, data, first_chunk_len);

    size_t second_chunk_len = len - first_chunk_len;
    memcpy(rb->memory, data, second_chunk_len);

    rb->start = second_chunk_len;
    rb->consumed += len;

    return true;
}

static inline size_t ringbuf_get_read_start(ringbuf_t *rb)
{
    return rb->consumed > rb->start
         ? rb->capacity - rb->consumed + rb->start
         : rb->start - rb->consumed;
}

bool ringbuf_get(ringbuf_t *rb, void *data, size_t len)
{
    if (rb->consumed < len) {
        return false;
    }

    size_t first_chunk_len = rb->capacity - ringbuf_get_read_start(rb);
    if (first_chunk_len > len) {
        memcpy(data, (uint8_t *)rb->memory + rb->capacity - first_chunk_len, len);
        rb->consumed -= len;
        return true;
    }

    memcpy(data, (uint8_t *)rb->memory + rb->capacity - first_chunk_len, first_chunk_len);

    size_t second_chunk_len = len - first_chunk_len;
    memcpy(data, rb->memory, second_chunk_len);

    rb->consumed -= len;
    return true;
}

bool ringbuf_put_u8 (ringbuf_t *rb, uint8_t  x) { return ringbuf_put(rb, &x, sizeof(x)); }
bool ringbuf_put_u16(ringbuf_t *rb, uint16_t x) { return ringbuf_put(rb, &x, sizeof(x)); }
bool ringbuf_put_u32(ringbuf_t *rb, uint32_t x) { return ringbuf_put(rb, &x, sizeof(x)); }
bool ringbuf_put_u64(ringbuf_t *rb, uint64_t x) { return ringbuf_put(rb, &x, sizeof(x)); }

bool ringbuf_put_string(ringbuf_t *rb, const char *str, size_t length)
{
    return ringbuf_put(rb, (void *)str, length + 1);
}

bool ringbuf_get_u8 (ringbuf_t *rb, uint8_t  *x) { return ringbuf_get(rb, x, sizeof(*x)); }
bool ringbuf_get_u16(ringbuf_t *rb, uint16_t *x) { return ringbuf_get(rb, x, sizeof(*x)); }
bool ringbuf_get_u32(ringbuf_t *rb, uint32_t *x) { return ringbuf_get(rb, x, sizeof(*x)); }
bool ringbuf_get_u64(ringbuf_t *rb, uint64_t *x) { return ringbuf_get(rb, x, sizeof(*x)); }

bool ringbuf_get_string(ringbuf_t *rb, char *out)
{
    size_t first_chunk_len = rb->capacity - ringbuf_get_read_start(rb);
    char *ptr = (char *)rb->memory + rb->capacity - first_chunk_len;
    size_t read = 0;

    while (*ptr != '\0') {
        *out++ = *ptr++;
        ++read;
    }

    rb->consumed -= read + /* NULL terminator */ 1;

    return true;
}

void *walić_kwas(void *s, int c, size_t n)
{
    return memset(s, c, n);
}

int main() {
    size_t buf_size = 10 * sizeof(uint32_t);
    ringbuf_t rbuf;
    char mem[1024];
    assert(ringbuf_create(&rbuf, mem, 1024));

    /// Fill in all buffer.
    for (uint32_t i = 1; i <= 10; ++i) {
        assert(ringbuf_put_u32(&rbuf, i));
        assert(rbuf.consumed == i * sizeof(uint32_t));
    }

    /// Get each element from buffer.
    for (uint32_t i = 1; i <= 10; ++i) {
        uint32_t value;
        assert(ringbuf_get_u32(&rbuf, &value));
        assert(value == i);
        assert(rbuf.consumed == buf_size - i * sizeof(uint32_t));
    }

    uint32_t value;
    /// False, all consumed stuff is retrieved.
    assert(!ringbuf_get_u32(&rbuf, &value));
    assert(ringbuf_put_u32(&rbuf, 10));
    /// Pass, we can get again.
    assert(ringbuf_get_u32(&rbuf, &value));
    assert(value == 10);

    char out[4];
    ringbuf_put_string(&rbuf, "123", 3);
    ringbuf_put_string(&rbuf, "456", 3);
    ringbuf_put_string(&rbuf, "abc", 3);
    ringbuf_put_string(&rbuf, "def", 3);
    ringbuf_put_string(&rbuf, "ghj", 3);

    for (int i = 0; i < 5; ++i) {
        walić_kwas(out, 0, 4);
        ringbuf_get_string(&rbuf, out);
        printf("Out: %s\n", out);
    }
}
