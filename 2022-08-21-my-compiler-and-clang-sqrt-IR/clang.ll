define dso_local float @__sqrt(float noundef %0) #0 {
  %2 = alloca float, align 4
  %3 = alloca float, align 4
  %4 = alloca float, align 4
  %5 = alloca float, align 4
  %6 = alloca float, align 4
  store float %0, float* %2, align 4
  store float 0.000000e+00, float* %3, align 4
  %7 = load float, float* %2, align 4
  store float %7, float* %4, align 4
  %8 = load float, float* %4, align 4
  %9 = fpext float %8 to double
  %10 = fdiv double %9, 2.000000e+00
  %11 = fptrunc double %10 to float
  store float %11, float* %5, align 4
  br label %12

12:                                               ; preds = %16, %1
  %13 = load float, float* %5, align 4
  %14 = load float, float* %3, align 4
  %15 = fcmp une float %13, %14
  br i1 %15, label %16, label %29

16:                                               ; preds = %12
  %17 = load float, float* %5, align 4
  store float %17, float* %3, align 4
  %18 = load float, float* %4, align 4
  %19 = load float, float* %3, align 4
  %20 = fdiv float %18, %19
  store float %20, float* %6, align 4
  %21 = load float, float* %3, align 4
  %22 = load float, float* %6, align 4
  %23 = fadd float %22, %21
  store float %23, float* %6, align 4
  %24 = load float, float* %6, align 4
  %25 = fpext float %24 to double
  %26 = fdiv double %25, 2.000000e+00
  %27 = fptrunc double %26 to float
  store float %27, float* %6, align 4
  %28 = load float, float* %6, align 4
  store float %28, float* %5, align 4
  br label %12, !llvm.loop !6

29:                                               ; preds = %12
  %30 = load float, float* %5, align 4
  ret float %30
}
