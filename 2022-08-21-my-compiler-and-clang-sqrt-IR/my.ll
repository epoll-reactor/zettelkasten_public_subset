define float @sqrt(float %value) {
entry:
  %value1 = alloca float, align 4
  store float %value, float* %value1, align 4
  %tmp = alloca float, align 4
  store float 0.000000e+00, float* %tmp, align 4
  %value2 = load float, float* %value1, align 4
  %number = alloca float, align 4
  store float %value2, float* %number, align 4
  %number3 = load float, float* %number, align 4
  %0 = fdiv float %number3, 2.000000e+00
  %root = alloca float, align 4
  store float %0, float* %root, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %root4 = load float, float* %root, align 4
  %tmp5 = load float, float* %tmp, align 4
  %1 = fcmp one float %root4, %tmp5
  br i1 %1, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %tmp6 = load float, float* %tmp, align 4
  %root7 = load float, float* %root, align 4
  store float %root7, float* %tmp, align 4
  %number8 = load float, float* %number, align 4
  %tmp9 = load float, float* %tmp, align 4
  %2 = fdiv float %number8, %tmp9
  %tmp_value = alloca float, align 4
  store float %2, float* %tmp_value, align 4
  %tmp_value10 = load float, float* %tmp_value, align 4
  %tmp11 = load float, float* %tmp, align 4
  %3 = fadd float %tmp_value10, %tmp11
  store float %3, float* %tmp_value, align 4
  %tmp_value12 = load float, float* %tmp_value, align 4
  %4 = fdiv float %tmp_value12, 2.000000e+00
  store float %4, float* %tmp_value, align 4
  %root13 = load float, float* %root, align 4
  %tmp_value14 = load float, float* %tmp_value, align 4
  store float %tmp_value14, float* %root, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %root15 = load float, float* %root, align 4
  ret float %root15
}
