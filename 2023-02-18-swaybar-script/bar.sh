
keyboard_input_name="1:1:AT_Translated_Set_2_keyboard"

date_and_week=$(date "+%Y/%m/%d")
current_time=$(date "+%H:%M")

battery_charge=$(cat /sys/class/power_supply/BAT0/status)
battery_status=$(cat /sys/class/power_supply/BAT0/status)

audio_volume=$(awk -F"[][]" '/Left:/ { print $2 }' <(amixer sget Master))

network=$(ip route get 1.1.1.1 | grep -Po '(?<=dev\s)\w+' | cut -f1 -d ' ')

language=$(swaymsg -r -t get_inputs | awk '/1:1:AT_Translated_Set_2_keyboard/;/xkb_active_layout_name/' | grep -A1 '\b1:1:AT_Translated_Set_2_keyboard\b' | grep "xkb_active_layout_name" | awk -F '"' '{print $4}')

echo "⌨ $language | $network_active $network | $audio_active $audio_volume | $battery_pluggedin $battery_charge | $date_and_week 🕘 $current_time"
