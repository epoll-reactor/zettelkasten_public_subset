#include <vector>
#include <map>
#include <set>
#include <string>
#include <algorithm>
#include <fstream>

/*****************************************************/
/* AST                                               */
/*****************************************************/

class ast_number;
class ast_symbol;
class ast_assign;
class ast_if;
class ast_for;
class ast_binary;
class ast_node;
class ir_node;
class ir_branch;
class ir_assign;

class ast_visitor {
public:
  virtual void visit(ast_number *expr) = 0;
  virtual void visit(ast_symbol *expr) = 0;
  virtual void visit(ast_assign *expr) = 0;
  virtual void visit(ast_if *expr) = 0;
  virtual void visit(ast_for *expr) = 0;
  virtual void visit(ast_binary *expr) = 0;
};

class ir_visitor {
public:
  virtual void visit(ir_branch *) = 0;
  virtual void visit(ir_assign *) = 0;
};

class ast_node {
public:
  virtual ~ast_node() = default;

  virtual std::string string() {
    return "";
  }

  virtual void accept(ast_visitor *visitor) = 0;
};

class ast_number : public ast_node {
public:
  int value_;

  ast_number(int value)
    : value_(value) {}

  std::string string() override {
    return std::to_string(value_);
  }

  void accept(ast_visitor *visitor) override {
    visitor->visit(this);
  }
};

class ast_symbol : public ast_node {
public:
  std::string name_;

  ast_symbol(std::string name)
    : name_(name)
    , ssa_index_(0) {}

  void set_ssa_index(int index) {
    ssa_index_ = index;
  };

  std::string string() override {
    return name_ + "#" + std::to_string(ssa_index_);
  }

  void accept(ast_visitor *visitor) override {
    visitor->visit(this);
  }

private:
  int ssa_index_;
};

class ast_assign : public ast_node {
public:
  ast_symbol *variable_;
  ast_node *expr_;

  ast_assign(ast_symbol *variable, ast_node *expr)
    : variable_(variable)
    , expr_(expr) {}

  std::string variable() {
    return variable_->string();
  }

  void accept(ast_visitor *visitor) override {
    visitor->visit(this);
  }
};

class ast_if : public ast_node {
public:
  ast_node *cond_, *then_, *else_;

  ast_if(ast_node *cond, ast_node *then, ast_node *else_part)
    : cond_(cond), then_(then), else_(else_part)  {}

  void accept(ast_visitor *visitor) override {
    visitor->visit(this);
  }
};

class ast_for : public ast_node {
public:
  ast_symbol *index_;
  ast_node *start, *end, *step, *body;

  ast_for(
    std::string  index,
    ast_node    *start,
    ast_node    *end,
    ast_node    *step,
    ast_node    *body
  ) : index_(new ast_symbol(index))
    , start(start)
    , end(end)
    , step(step)
    , body(body) {}

  void accept(ast_visitor *visitor) override {
    visitor->visit(this);
  }
};

class ast_binary: public ast_node {
public:
  char op;
  ast_node *lhs, *rhs;

  ast_binary(char op, ast_node *lhs, ast_node *rhs)
    : op(op)
    , lhs(lhs)
    , rhs(rhs) {}

  std::string string() override {
    return lhs->string() + " " + op + " " + rhs->string();
  }

  void accept(ast_visitor *visitor) override {
    visitor->visit(this);
  }
};

/*****************************************************/
/* Basic cfg block                                   */
/*****************************************************/

class cfg_block {
public:
  cfg_block(int index, std::string label)
    : index_(index), label_(label) {}

  std::string string();

  void add_stmt(ir_node *);
  static void add_link(cfg_block *pred, cfg_block *succ);

  std::vector<ir_node *> stmts;
  std::vector<cfg_block *> succs;
  std::vector<cfg_block *> preds;

  cfg_block *dominator;
  std::vector<cfg_block *> submissive;

private:
  int index_;
  std::string label_;
};

std::string cfg_block::string() {
  return "cfg#" + std::to_string(index_) + "(" + label_ + ")";
}

void cfg_block::add_stmt(ir_node *stmt) {
  stmts.push_back(stmt);
}

void cfg_block::add_link(cfg_block *pred, cfg_block *succ) {
  if (std::find(succ->preds.begin(), succ->preds.end(), pred) == succ->preds.end())
    succ->preds.push_back(pred);
  if (std::find(pred->succs.begin(), pred->succs.end(), succ) == pred->succs.end())
    pred->succs.push_back(succ);
}

/*****************************************************/
/* Control Flow Graph                                */
/*****************************************************/

class cfg {
public:
  std::vector<cfg_block *> cfg_blocks;
  std::set<cfg_block *> get_dominance_frontier_for_subset(const std::set<cfg_block*> &);
  void commit();
  void add_block(cfg_block* block);

private:
  void pred_order_dfs(cfg_block *);
  void post_order_dfs(cfg_block *);
  void dom_dfs(cfg_block *);

  void compute_pred_order();
  void compute_post_order();
  void compute_dominator_tree();
  void compute_base_dominance_frontier();
  std::set<cfg_block *> get_merged_dominance_frontier_for_subset(const std::set<cfg_block*> &);

  std::vector<cfg_block *> pred_order_;
  std::vector<cfg_block *> post_order_;
  std::map<cfg_block *, int> visit_map_;
  std::map<cfg_block *, std::set<cfg_block *>> dominance_frontier_;
};

void cfg::add_block(cfg_block *block) {
  cfg_blocks.push_back(block);
}

void cfg::compute_pred_order() {
  if (pred_order_.empty()) {
    visit_map_.clear();
    pred_order_dfs(cfg_blocks.front());
  }
}

void cfg::pred_order_dfs(cfg_block *block) {
  visit_map_[block] = 1;
  pred_order_.push_back(block);
  for (auto *next : block->succs)
    if (!visit_map_[next])
      pred_order_dfs(next);
}

void cfg::compute_post_order() {
  if (post_order_.empty()) {
    visit_map_.clear();
    post_order_dfs(cfg_blocks.front());
  }
}

void cfg::post_order_dfs(cfg_block *block) {
  visit_map_[block] = 1;
  for (auto *next : block->succs)
    if (!visit_map_[next])
      post_order_dfs(next);
  post_order_.push_back(block);
}

void cfg::compute_dominator_tree() {
  if (!pred_order_.empty())
    compute_pred_order();

  for (auto *it : pred_order_) {
    visit_map_.clear();
    visit_map_[it] = 1;
    dom_dfs(pred_order_.front());
    for (auto *jt : pred_order_)
      if (!visit_map_[jt])
        jt->dominator = it;
  }

  for (auto *block : cfg_blocks) {
    if (cfg_block *dominator = block->dominator)
      dominator->submissive.push_back(block);
  }
}

void cfg::dom_dfs(cfg_block *block) {
  if (!visit_map_[block]) {
    visit_map_[block] = 1;
    for (auto *next : block->succs)
      if (!visit_map_[next])
        dom_dfs(next);
  }
}

void cfg::compute_base_dominance_frontier() {
  if (!dominance_frontier_.empty())
    return;

  for (auto *block : cfg_blocks)
    dominance_frontier_[block];

  for (auto *x : post_order_) {
    for (auto *y : x->succs)
      if (y->dominator != x)
        dominance_frontier_.at(x).insert(y);

    for (auto *z : x->submissive)
      for (auto *y : dominance_frontier_.at(z))
        if (y->dominator != x)
          dominance_frontier_.at(x).insert(y);
  }
}

std::set<cfg_block *> cfg::get_merged_dominance_frontier_for_subset(const std::set<cfg_block*> &subset) {
  if (dominance_frontier_.empty())
    compute_base_dominance_frontier();

  std::set<cfg_block *> merged;

  for (cfg_block *b : subset) {
    std::set<cfg_block *> df = dominance_frontier_.at(b);
    merged.insert(df.begin(), df.end());
  }

  return merged;
}

std::set<cfg_block *> cfg::get_dominance_frontier_for_subset(const std::set<cfg_block*> &subset) {
  std::set<cfg_block *> result;
  std::set<cfg_block *> dfp = get_merged_dominance_frontier_for_subset(subset);
  bool changed = true;

  while (changed) {
    changed = false;
    dfp.insert(subset.begin(), subset.end());
    dfp = get_merged_dominance_frontier_for_subset(dfp);
    if (result != dfp) {
      result = dfp;
      changed = true;
    }
  }

  return result;
}

void cfg::commit() {
  compute_pred_order();
  compute_post_order();
  compute_dominator_tree();
  compute_base_dominance_frontier();
}

/*****************************************************/
/* IR statements                                     */
/*****************************************************/

enum ir_type { ASSIGN = 0, BRANCH = 1, PHI = 2 };

class ir_node {
public:
  virtual ~ir_node() = default;

  ir_type type;

  ir_node(ir_type type) : type(type) {}

  virtual std::string dump() = 0;
  virtual void accept(ir_visitor *) = 0;
};

class ir_assign : public ir_node {
public:
  ir_assign(ast_symbol *variable, ast_node *rhs)
    : ir_node(ASSIGN), variable_(variable), rhs_(rhs) {}

  std::string dump() override {
    return variable_->string() + " = " + rhs_->string();
  }

  void accept(ir_visitor *visitor) override {
    visitor->visit(this);
  }

  ast_symbol *variable_;
  ast_node *rhs_;
};

class ir_branch : public ir_node {
public:

  std::string dump() override {
    std::string result = "Branch";

    if (is_conditional_)
      result += " (" + cond_->string() + ")";

    result += " on true to " + then_block_->string();

    if (is_conditional_)
      result += ", on false to " + else_block_->string();

    return result;
  }

  ir_branch(ast_node *cond, cfg_block *then_block, cfg_block *else_block)
    : ir_branch(cond, then_block, else_block, true) {}

  ir_branch(cfg_block *block)
    : ir_branch(nullptr, block, nullptr, false) {}

  void accept(ir_visitor *visitor) override {
    visitor->visit(this);
  }

  bool is_conditional_;
  cfg_block *then_block_;
  cfg_block *else_block_;
  ast_node *cond_;

private:
  ir_branch(ast_node *cond, cfg_block *then_block, cfg_block *else_block, bool is_conditional)
    : ir_node(BRANCH)
    , is_conditional_(is_conditional)
    , then_block_(then_block)
    , else_block_(else_block)
    , cond_(cond) {}
};

class ir_phi_node : public ir_node {
public:
  ir_phi_node(
    ast_symbol                          *var,
    std::map<cfg_block *, ast_symbol *>  var_map
  ) : ir_node(PHI)
    , variable(var)
    , block_var_mapping(var_map) {}

  std::string dump() override {
    std::string args;
    for (auto [block, symbol] : block_var_mapping)
      args += block->string() + ":" + symbol->string() + ", ";

    if (!args.empty()) {
      args.pop_back();
      args.pop_back();
    }

    return variable->string() + " = φ(" + args + ")";
  }

  void accept(ir_visitor *) override {}

  ast_symbol *variable;
  std::map<cfg_block *, ast_symbol *> block_var_mapping;
};

/*****************************************************/
/* variable search visitor                           */
/*****************************************************/

/// This used for getting all variables in AST or IR statement recursively.
class variable_search_visitor : public ast_visitor, public ir_visitor {
public:
  void visit(ast_number *) override {}
  void visit(ast_if *) override {}
  void visit(ast_for *) override {}
  void visit(ast_symbol *) override;
  void visit(ast_assign *) override;
  void visit(ast_binary *) override;
  void visit(ir_branch *) override;
  void visit(ir_assign *) override;
  std::set<ast_symbol *> all_vars_used_in_stmt(ir_node *);

private:
  std::set<ast_symbol *> variables_;
};

std::set<ast_symbol *> variable_search_visitor::all_vars_used_in_stmt(ir_node *stmt) {
  variables_.clear();
  stmt->accept(this);
  return variables_;
}

void variable_search_visitor::visit(ir_branch *stmt) {
  if (stmt->cond_)
    stmt->cond_->accept(this);
}
void variable_search_visitor::visit(ir_assign *stmt) {
  stmt->rhs_->accept(this);
}
void variable_search_visitor::visit(ast_symbol *stmt) {
  variables_.insert(stmt);
}
void variable_search_visitor::visit(ast_assign *stmt) {
  variables_.insert(stmt->variable_);
  stmt->expr_->accept(this);
}
void variable_search_visitor::visit(ast_binary *stmt) {
  stmt->lhs->accept(this);
  stmt->rhs->accept(this);
}

/*****************************************************/
/* SSA builder                                       */
/*****************************************************/

class ssa_builder {
public:
  ssa_builder(cfg *graph) : cfg_(graph) {}

  void rename_var_to_ssa(std::string variable);

private:
  int counter_;
  std::vector<int> stack_;
  variable_search_visitor variable_visitor_;

  void traverse(cfg_block *block, std::string variable);

  cfg *cfg_;
};

void ssa_builder::rename_var_to_ssa(std::string variable) {
  counter_ = 0;
  stack_.clear();
  traverse(cfg_->cfg_blocks.front(), variable);
}

void ssa_builder::traverse(cfg_block *block, std::string variable) {
  for (const auto &stmt : block->stmts) {
    if (stmt->type == PHI) {
      ir_phi_node *phi = static_cast<ir_phi_node *>(stmt);
      if (phi->variable->name_ == variable) {
        phi->variable->set_ssa_index(counter_);
        stack_.push_back(counter_++);
      }
    }
    else {
      for (auto *var : variable_visitor_.all_vars_used_in_stmt(stmt))
        if (var->name_ == variable)
          var->set_ssa_index(stack_.back());
    }

    if (stmt->type == ASSIGN) {
      ir_assign *assignment = static_cast<ir_assign *>(stmt);
      if (assignment->variable_->name_ == variable) {
        assignment->variable_->set_ssa_index(counter_);
        stack_.push_back(counter_++);
      }
    }
  }

  for (auto *succ : block->succs)
    for (const auto &stmt : succ->stmts)
      if (stmt->type == PHI) {
        ir_phi_node *phi = static_cast<ir_phi_node *>(stmt);
        if (phi->block_var_mapping.count(block) && phi->block_var_mapping[block]->name_ == variable)
          phi->block_var_mapping[block]->set_ssa_index(stack_.back());
      }

  for (auto *child : block->submissive)
    traverse(child, variable);

  for (const auto &stmt : block->stmts) {
    if (stmt->type == ASSIGN) {
      ir_assign *assignment = static_cast<ir_assign *>(stmt);
      if (assignment->variable_->name_ == variable)
        stack_.pop_back();
    }
  }
}

/*****************************************************/
/* cfg builder                                       */
/*****************************************************/

class cfg_builder : public ast_visitor {
public:
  cfg_builder();

  void visit(ast_number *) override {}
  void visit(ast_symbol *) override {}
  void visit(ast_for *) override {}
  void visit(ast_assign *) override;
  void visit(ast_if *) override;
  void visit(ast_binary *) override;

  void traverse(ast_node *);
  void commit_ssa();

  cfg *get_cfg() {
    return cfg_;
  }

  std::map<std::string, std::set<cfg_block *>> block_var_mapping_;

private:
  cfg_block *create_block(std::string label_);
  void create_branch(cfg_block *target);
  void create_conditional_branch(ast_node *cond_, cfg_block *then_block, cfg_block *else_block);

  void insert_phi_nodes();
  void build_ssa();

  cfg *cfg_;

  cfg_block *current_cfg_;
};

cfg_block* cfg_builder::create_block(std::string label) {
  int next_index = static_cast<int>(cfg_->cfg_blocks.size());
  auto *block = new cfg_block(next_index, label);
  cfg_->add_block(block);
  return block;
}

cfg_builder::cfg_builder()
  : cfg_(new cfg)
  , current_cfg_(create_block("Entry")) {}

void cfg_builder::create_branch(cfg_block *target) {
  cfg_block::add_link(current_cfg_, target);
  current_cfg_->add_stmt(new ir_branch(target));
}

void cfg_builder::create_conditional_branch(ast_node *cond, cfg_block *then_block, cfg_block *else_block) {
  cfg_block::add_link(current_cfg_, then_block);
  cfg_block::add_link(current_cfg_, else_block);
  current_cfg_->add_stmt(new ir_branch(cond, then_block, else_block));
}

void cfg_builder::traverse(ast_node *expr) {
  expr->accept(this);
}

void cfg_builder::visit(ast_assign *expr) {
  block_var_mapping_[expr->variable_->name_].insert(current_cfg_);
  current_cfg_->add_stmt(new ir_assign(expr->variable_, expr->expr_));
}

void cfg_builder::visit(ast_if *expr) {
  cfg_block *then_block = create_block("Then");
  cfg_block *else_block = create_block("Else");
  cfg_block *merge_block = create_block("IfContinue");

  create_conditional_branch(expr->cond_, then_block, else_block);

  current_cfg_ = then_block;
  traverse(expr->then_);
  create_branch(merge_block);

  current_cfg_ = else_block;
  traverse(expr->else_);
  create_branch(merge_block);

  current_cfg_ = merge_block;
}

void cfg_builder::visit(ast_binary *expr) {
  traverse(expr->lhs);
  traverse(expr->rhs);
}

void cfg_builder::commit_ssa() {
  cfg_->commit();
  insert_phi_nodes();
  build_ssa();
}

void cfg_builder::insert_phi_nodes() {
  for (const auto &[variable_name, assigned_blocks] : block_var_mapping_) {
    std::set<cfg_block *> dominance_frontier_ = cfg_->get_dominance_frontier_for_subset(assigned_blocks);
    for (auto *block : dominance_frontier_) {
      std::map<cfg_block *, ast_symbol *> block_var_mapping;
      for (auto *predecessor : block->preds)
        block_var_mapping[predecessor] = new ast_symbol(variable_name);
      auto phi = new ir_phi_node(
        new ast_symbol(variable_name), block_var_mapping);
      block->stmts.insert(block->stmts.begin(), phi);
    }
  }
}

void cfg_builder::build_ssa() {
  ssa_builder builder(cfg_);
  for (const auto &[name, blocks] : block_var_mapping_) {
    builder.rename_var_to_ssa(name);
  }
}

/*****************************************************/
/* cfg Printer                                       */
/*****************************************************/
std::string cfg_to_dot(cfg *cfg_) {
  std::string out_graph;

  out_graph += "digraph G {\n";
  out_graph += "  node[shape=box];\n";

  for (auto *block : cfg_->cfg_blocks) {
    for (auto *successor : block->succs) {
      auto dump = [](cfg_block *block) {
        std::string result;

        result += block->string();
        result += "\n";
        for (const auto &stmt : block->stmts)
          result += stmt->dump() + "\n";

        return result;
      };

      out_graph += "\t\"";
      out_graph += dump(block);
      out_graph += "\" -> \"";
      out_graph += dump(successor);
      out_graph += "\"\n";
    }
  }

  return out_graph + "}\n";
}

void run_ssa_form_tests() {
  std::vector<ast_node *> expressions;
  expressions.push_back(new ast_assign(new ast_symbol("first"), new ast_number(1)));
  expressions.push_back(new ast_assign(new ast_symbol("second"), new ast_number(2)));
  expressions.push_back(new ast_assign(new ast_symbol("third"), new ast_number(3)));
  expressions.push_back(
    new ast_if(
      // cond_.
      new ast_binary(
        '+',
        new ast_binary(
          '+',
          new ast_number(2002),
          new ast_number(3003)
        ),
        new ast_number(4004)
      ),
      // Then body.
      new ast_assign(new ast_symbol("first"), new ast_symbol("second")),
      // Else body.
      new ast_if(
        // cond_.
        new ast_binary(
          '/',
          new ast_binary(
            '*',
            new ast_number(5005),
            new ast_number(6006)
          ),
          new ast_number(7007)
        ),
        // Then body.
        new ast_assign(new ast_symbol("first"), new ast_symbol("second")),
        // Else body.
        new ast_assign(new ast_symbol("second"), new ast_symbol("third"))
      )
    )
  );
  expressions.push_back(new ast_assign(new ast_symbol("first"), new ast_symbol("third")));
  expressions.push_back(new ast_assign(new ast_symbol("second"), new ast_symbol("second")));
  expressions.push_back(new ast_assign(new ast_symbol("third"), new ast_symbol("first")));

  cfg_builder builder;
  for (auto *expr : expressions)
    builder.traverse(expr);

  builder.commit_ssa();

  std::ofstream("cfg.gv") << cfg_to_dot(builder.get_cfg());
  system("dot -Tjpg cfg.gv -o cfg.jpg && sxiv cfg.jpg");
  std::remove("cfg.gv");
}
