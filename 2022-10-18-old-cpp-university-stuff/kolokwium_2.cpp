#include <iostream>
#include <random>
#include <ctime>
#include <cassert>

class Zwierzeta {
public:
  virtual ~Zwierzeta() {}
  virtual std::string Drukuj() const = 0;
};

class Zyrafy : public Zwierzeta {
public:
  std::string Drukuj() const override {
    return "Zyraf";
  }
};

class Slonie : public Zwierzeta {
public:
  std::string Drukuj() const override {
    return "Slonie";
  }
};

class Lwy : public Zwierzeta {
public:
  std::string Drukuj() const override {
    return "Lwy";
  }
};

class Malpy : public Zwierzeta {
public:
  std::string Drukuj() const override {
    return "Malpy";
  }
};

class Wagon {
public:
  Wagon(int Ilosc_, Zwierzeta *Zwierze_)
    : Ilosc(Ilosc_)
    , Zwierze(Zwierze_) {}

  int GetIlosc() const { return Ilosc; }
  Zwierzeta *GetZwierzeta() const { return Zwierze; }

private:
  int Ilosc;
  Zwierzeta *Zwierze;
};

class Pociag {
public:
  Pociag() = default;

  Pociag(const Pociag &Inny)
    : Wagony(Inny.Wagony) {}

  Pociag &operator<<(Wagon W) {
    Wagony.push_back(W);
    return *this;
  }

  void Odepnij(int Index) {
    Wagony.erase(Wagony.begin() + Index);
  }

  void coNajwyzej(int Ilosc) {
    for (auto It = Wagony.begin(); It != Wagony.end(); ) {
      if (It->GetIlosc() > Ilosc) {
        It = Wagony.erase(It);
      } else {
        ++It;
      }
    }
  }

  friend std::ostream &operator<<(std::ostream &Stream, const Pociag &P) {
    for (Wagon W : P.Wagony) {
      Stream << "[" << W.GetZwierzeta()->Drukuj() << "," << W.GetIlosc() << "]";
      Stream << "<->";
    }
    return Stream << std::endl;
  }

private:
  std::vector<Wagon> Wagony;
};

int main() {
  Zwierzeta *Z1 = new Lwy;
  Zwierzeta *Z2 = new Slonie;

  Wagon W1(1, Z1);
  Wagon W2(2, Z2);
  Wagon W3(3, Z2);
  Wagon W4(4, Z2);
  Wagon W5(5, Z2);

  Pociag P;
  P << W1 << W2 << W3 << W4 << W5;

  std::cout << P;

  P.Odepnij(0);

  std::cout << P;

  P.coNajwyzej(3);

  std::cout << P;

  delete Z1;
  delete Z2;
}
