#include <iostream>
#include <random>
#include <ctime>
#include <cassert>

class Kostka {
public:
  Kostka(int IloscKostek_)
    : IloscKostek(IloscKostek_) {
    ++Stworzono;
  }

  Kostka(const Kostka &Inny)
    : IloscKostek(Inny.IloscKostek) {
    ++Stworzono;
  }

  Kostka &operator=(const Kostka &Inny) {
    IloscKostek = Inny.IloscKostek;
    return *this;
  }

  virtual ~Kostka() {
    --Stworzono;
  }

  virtual void Losuj() const {
    srand(time(nullptr));
    std::cout << (std::rand() % IloscKostek + 1) << std::endl;
  }

  static int GetIle() {
    return Stworzono;
  }

  bool operator==(const Kostka &Inny) const {
    return IloscKostek == Inny.IloscKostek;
  }

//  friend bool operator==(const Kostka &Left, const Kostka &Right) {
//    return Left.IloscKostek == Right.IloscKostek;
//  }

//  friend bool operator==(const Kostka &Left, const Kostka &Right);

  friend std::istream &operator>>(std::istream &Stream, Kostka &K);

protected:
  int IloscKostek;
  static int Stworzono;
};

int Kostka::Stworzono = 0;

//bool operator==(const Kostka &Left, const Kostka &Right) {
//  return Left.IloscKostek == Right.IloscKostek;
//}

std::istream &operator>>(std::istream &Stream, Kostka &K) {
  return Stream >> K.IloscKostek;
}

class KolorowaKostka : public Kostka {
public:
  KolorowaKostka(int Ile, std::string Imie_)
    : Kostka(Ile)
    , Imie(Imie_) {}

  KolorowaKostka(const KolorowaKostka &Inny)
    : Kostka(Inny.IloscKostek)
    , Imie(Inny.Imie) {}

  KolorowaKostka &operator=(const KolorowaKostka &Inny) {
    IloscKostek = Inny.IloscKostek;
    Imie = Inny.Imie;
    return *this;
  }

  virtual void Losuj() const override {
    srand(time(nullptr));
    std::cout << Imie << ": " << (std::rand() % Kostka::IloscKostek + 1) << std::endl;
  }

  bool operator==(const KolorowaKostka &Inny) const {
    return (Imie == Inny.Imie) && (IloscKostek == Inny.IloscKostek);
  }

private:
  std::string Imie;
};

//template <typename String, typename KostkaT>
//class KolorowaKostkaTP {
//public:
//  KolorowaKostkaTP(std::string Imie_, KostkaT Kostka_)
//    : Imie(Imie_)
//    , Kostka(Kostka_) {}
//
//  void Losuj() const {
//    std::cout << Imie << ": ";
//    Kostka.Losuj();
//  }
//
//private:
//  std::string Imie;
//  KostkaT Kostka;
//};

template <typename String, typename KostkaT>
class KolorowaKostkaTP : KostkaT {
public:
  KolorowaKostkaTP(std::string Imie_, KostkaT Kostka)
    : KostkaT(Kostka)
    , Imie(Imie_) {}

  virtual void Losuj() const override {
    std::cout << Imie << ": " << (std::rand() % Kostka::IloscKostek + 1) << std::endl;
  }

private:
  std::string Imie;
};

int main() {
  {
    Kostka* K1 = new Kostka(10);
    Kostka* K2 = new KolorowaKostka(10, "Imie1");
    KolorowaKostka* K3 = new KolorowaKostka(20, "Imie2");

    std::cin >> *K1;

    K1->Losuj();
    K2->Losuj();

    assert(!(*K2 == *K3));

    *K2 = *K3;

    assert(*K2 == *K3);

    K2->Losuj();

    std::cout << "Stoworzono " << Kostka::GetIle() << std::endl;
    delete K1;
    delete K2;
    delete K3;
    std::cout << "Stoworzono " << Kostka::GetIle() << std::endl;
  } {
    Kostka K(1);
    KolorowaKostkaTP<std::string, Kostka> KTemplate("Imie", K);
    KTemplate.Losuj();
  }
}
