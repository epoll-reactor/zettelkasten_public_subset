#include <iostream>
#include <vector>
#include <fstream>
#include <numeric>
#include <algorithm>
#include <random>
#include <chrono>

int64_t partition(int64_t* array, int64_t low, int64_t high) {
  int64_t pivot = array[high];
  int64_t i = (low - 1);

  for (int64_t j = low; j < high; ++j) {
    if (array[j] <= pivot) {
      ++i;
      std::swap(array[i], array[j]);
    }
  }
  std::swap(array[i + 1], array[high]);
  return i + 1;
}

int64_t random_partition(int64_t* array, int64_t low, int64_t high) {
  srand(time(nullptr));
  int64_t random = low + rand() % (high - low);
  std::swap(array[random], array[high]);
  return partition(array, low, high);
}

int64_t median_of_3_partition(int64_t* array, int64_t low, int64_t high) {
  if ((high - low) < 3) {
    return partition(array, low, high);
  }
  int64_t mid = (low + high) / 2;
  if (array[high] < array[low]) {
    std::swap(array[high], array[low]);
  }
  if (array[mid] < array[low]) {
    std::swap(array[mid], array[low]);
  }
  if (array[high] < array[mid]) {
    std::swap(array[high], array[mid]);
  }
  return partition(array, low, high);
}

void quick_sort(int64_t* array, int64_t low, int64_t high) {
  if (low >= high) return;
  int64_t part = partition(array, low, high);
  quick_sort(array, low, part - 1);
  quick_sort(array, part + 1, high);
}

void rand_quick_sort(int64_t* array, int64_t low, int64_t high) {
  if (low >= high) return;
  int64_t part = random_partition(array, low, high);
  rand_quick_sort(array, low, part - 1);
  rand_quick_sort(array, part + 1, high);
}

void median_of_3_quick_sort(int64_t* array, int64_t low, int64_t high) {
  if (low >= high) return;
  int64_t part = median_of_3_partition(array, low, high);
  median_of_3_quick_sort(array, low, part - 1);
  median_of_3_quick_sort(array, part + 1, high);
}

template <typename Function>
std::string time_measurement(Function function)
{
  auto start = std::chrono::high_resolution_clock::now();
  function();
  auto time_spent = std::chrono::high_resolution_clock::now() - start;
  return std::to_string(std::chrono::duration_cast<std::chrono::duration<float>>(time_spent).count());
}

void test(
  int64_t size,
  void(*sorting_fun)(int64_t*, int64_t, int64_t),
  void(*generator)(typename std::vector<int64_t>::iterator, typename std::vector<int64_t>::iterator),
  std::string report_path
) {
  std::ofstream out_file(report_path, std::ios::app);
  std::vector<int64_t> data(size);
  generator(data.begin(), data.end());

  std::string time_spent = time_measurement([&]{
  	sorting_fun(data.data(), 0, data.size() - 1);
  });
  
  out_file << size << " " << time_spent << std::endl;
  std::cout << size << " " << time_spent << std::endl;
  if (!std::is_sorted(data.begin(), data.end())) {
  	std::cerr << "Range isn't sorted!\n";
  	exit(-1);
  }
}

template <typename It>
void gen_big_random_ints(It begin, It end) {
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_int_distribution<> distrib(1'000'000, 10'000'000);
  while (begin != end) {
    *begin = distrib(gen);
  	++begin;
  }
}

template <typename It>
void gen_0_99_ints(It begin, It end) {
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_int_distribution<> distrib(0, 99);
  while (begin != end) {
    *begin = distrib(gen);
  	++begin;
  }
}

template <typename It>
void gen_ascending_order_ints(It begin, It end) {
  int64_t value = 0;
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_int_distribution<> distrib(0, 2);
  while (begin != end) {
    *begin = value + distrib(gen);
    ++value;
  	++begin;
  }
}

int main() {
  for (size_t i = 10000; i < 200000; i += 10000) {
  	test(i, quick_sort, gen_big_random_ints, "quick_sort_big_random_report.txt");
  }
  for (size_t i = 10000; i < 200000; i += 10000) {
  	test(i, quick_sort, gen_ascending_order_ints, "quick_sort_ascending_order_report.txt");
  }
  for (size_t i = 10000; i < 200000; i += 10000) {
  	test(i, quick_sort, gen_0_99_ints, "quick_sort_0_99_report.txt");
  }

  for (size_t i = 10000; i < 200000; i += 10000) {
    test(i, rand_quick_sort, gen_big_random_ints, "rand_quick_sort_big_random_report.txt");
  }
  for (size_t i = 10000; i < 200000; i += 10000) {
    test(i, rand_quick_sort, gen_ascending_order_ints, "rand_quick_ascending_order_sort_report.txt");
  }
  for (size_t i = 10000; i < 200000; i += 10000) {
    test(i, rand_quick_sort, gen_0_99_ints, "rand_quick_sort_0_99_report.txt");
  }

  for (size_t i = 10000; i < 200000; i += 10000) {
  	test(i, median_of_3_quick_sort, gen_big_random_ints, "median_of_3_quick_sort_big_random_report.txt");
  }
  for (size_t i = 10000; i < 200000; i += 10000) {
  	test(i, median_of_3_quick_sort, gen_ascending_order_ints, "median_of_3_quick_sort_ascending_order_report.txt");
  }
  for (size_t i = 10000; i < 200000; i += 10000) {
  	test(i, median_of_3_quick_sort, gen_0_99_ints, "median_of_3_quick_sort_0_99_report.txt");
  }
}
