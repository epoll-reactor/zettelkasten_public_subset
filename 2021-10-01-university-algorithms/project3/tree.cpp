#include <iostream>
#include <cassert>
#include <chrono>
#include <vector>
#include <fstream>
#include <random>
#include <iterator>

class tree_node {
public:
  std::string data_;
  tree_node* left_;
  tree_node* right_;
  int copies_;

  tree_node(std::string data = "") {
    left_ = nullptr;
    right_ = nullptr;
    data_ = std::move(data);
    copies_ = 1;
  }
};

class binary_tree {
private:
  tree_node* root_;
  int size_;
  void print_tree(tree_node*);
  void deallocate(tree_node*);
  int total_insertion_comparisons = 0;
  int total_search_comparisons = 0;
  int total_erase_comparisons = 0;

public:
  binary_tree();
  ~binary_tree();
  void print();
  bool find(std::string);
  void insert(std::string);
  void erase(std::string);
  int size();

  int get_insertion_comparisons();
  int get_search_comparisons();
  int get_erase_comparisons();
};

binary_tree::binary_tree() {
  root_ = new tree_node("");
  size_ = 1;
}

binary_tree::~binary_tree() {
  deallocate(root_);
}

void binary_tree::deallocate(tree_node* node) {
  if (node) {
    deallocate(node->left_);
    deallocate(node->right_);
    delete node;
  }
}

void binary_tree::print() {
  print_tree(root_);
  std::cout << std::endl;
}

void binary_tree::print_tree(tree_node* node) {
  if (node) {
    print_tree(node->left_);
    for (int i = 0; i < node->copies_; ++i)
      std::cout << node->data_ << " ";
    print_tree(node->right_);
  }
}

bool binary_tree::find(std::string key) {
  tree_node* curr = root_;
  while (curr && curr->data_ != key) {
    // 2 porownywania, != i >.
    total_search_comparisons += 2;
    if (curr->data_ > key) {
      curr = curr->left_;
    } else {
      curr = curr->right_;
    }
  }
  return curr != nullptr;
}

void binary_tree::insert(std::string key) {
  tree_node* node = root_;
  while (node) {
    ++total_insertion_comparisons;
    if (node->data_ == key) {
      node->copies_++;
      return;
    }
    ++total_insertion_comparisons;
    if (node->data_ > key && !node->left_) {
      node->left_ = new tree_node(std::move(key));
      ++size_;
      return;
    }
    ++total_insertion_comparisons;
    if (node->data_ < key && !node->right_) {
      node->right_ = new tree_node(std::move(key));
      ++size_;
      return;
    }
    ++total_insertion_comparisons;
    if (node->data_ > key) {
      node = node->left_;
    } else {
      node = node->right_;
    }
  }
}

void binary_tree::erase(std::string key) {
  tree_node* curr = root_;
  tree_node* parent = nullptr;

  while (curr && curr->data_ != key) {
    parent = curr;
    total_erase_comparisons += 2;
    if (curr->data_ > key) {
      curr = curr->left_;
    } else {
      curr = curr->right_;
    }
  }
  if (!curr) {
    return;
  }
  if (curr->copies_ > 1) {
    curr->copies_--;
    return;
  }
  if (!curr->left_) {
    // Zawieszamy zamiast curr jego prawe podrzewo.
    if (parent && parent->left_ == curr) {
      parent->left_ = curr->right_;
    }
    if (parent && parent->right_ == curr) {
      parent->right_ = curr->right_;
    }
    --size_;
    delete curr;
    return;
  }
  if (!curr->right_) {
    // Zawieszamy zamiast curr jego lewe podrzewo.
    if (parent && parent->left_ == curr) {
      parent->left_ = curr->left_;
    }
    if (parent && parent->right_ == curr) {
      parent->right_ = curr->left_;
    }
    --size_;
    delete curr;
    return;
  }
  // Element ma 2 potomki, wtedy zamiast curr wstawiamy najmniejszy
  // element z jego prawego poddrzewa.
  tree_node* replace = curr->right_;
  while (replace->left_) {
    replace = replace->left_;
  }
  std::string replace_value = replace->data_;
  erase(replace_value);
  curr->data_ = std::move(replace_value);
}

int binary_tree::get_erase_comparisons() { return total_erase_comparisons; }
int binary_tree::get_insertion_comparisons()  { return total_insertion_comparisons; }
int binary_tree::get_search_comparisons() { return total_search_comparisons; }

std::vector<std::string> generate_1_case(size_t size) {
  std::vector<std::string> values;

  static const char* words[] = {"ALA", "OLA", "AS"};
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_int_distribution<> distrib(0, 2);
  for (size_t i = 0; i < size; ++i) {
    values.emplace_back(words[distrib(gen)]);
  }
  return values;
}

std::vector<std::string> generate_2_case(size_t size) {
  std::vector<std::string> values;

  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_int_distribution<> distrib(1, 4);
  for (size_t i = 0; i < size; ++i) {
    std::string word;
    for (size_t j = 0, rand_length = distrib(gen); j < rand_length; ++j) {
      std::uniform_int_distribution<> letter_distrib(0, 26);
      word += ('a' + letter_distrib(gen));
    }
    values.push_back(std::move(word));
  }
  return values;
}

std::vector<std::string> generate_3_case(size_t size) {
  size_t counter = 0;
  std::string word;
  std::vector<std::string> words;
  words.reserve(size);
  /*! Musi byc stworzony w folderze z programem. */
  std::ifstream input_stream("polish_words.txt");
  while (input_stream >> word) {
    if (counter++ >= size)
      break;
  	words.push_back(std::move(word));
  }
  return words;
}

template <typename TestCase>
void test(
  std::vector<std::string> &&data,
  std::string &&report_path,
  TestCase &&test_case
) {
  std::ofstream out_file(report_path, std::ios::app);

  int op_count = test_case(std::move(data));

  out_file << data.size() << " " << op_count << std::endl;
  std::cout << data.size() << " " << op_count << std::endl;
}

void basic_test() {
  binary_tree tree;

  for (std::string i : {"50", "50", "150", "100", "25", "75", "125", "175", "65", "85"}) {
    tree.insert(i);
  }

  tree.print();
  assert(tree.find("50"));
  tree.erase("50");
  assert(tree.find("50"));
  tree.erase("50");
  assert(!tree.find("50"));
}

int main() {
  /*! Wstawianie. */
  for (size_t i = 100; i < 10000; i += 100) {
    test(generate_1_case(i), "tree_insert_report_1.txt", [](std::vector<std::string>&& values) {
      binary_tree tree;
      for (std::string value: values) {
        tree.insert(std::move(value));
      }
      return tree.get_insertion_comparisons();
    });
  }
  for (size_t i = 100; i < 10000; i += 100) {
    test(generate_2_case(i), "tree_insert_report_2.txt", [](std::vector<std::string>&& values) {
      binary_tree tree;
      for (std::string value: values) {
        tree.insert(std::move(value));
      }
      return tree.get_insertion_comparisons();
    });
  }
  for (size_t i = 100; i < 10000; i += 100) {
    test(generate_3_case(i), "tree_insert_report_3.txt", [](std::vector<std::string>&& values) {
      binary_tree tree;
      for (std::string value: values) {
          tree.insert(std::move(value));
      }
      return tree.get_insertion_comparisons();
    });
  }

  /*! Wyszukiwanie. */
  for (size_t i = 100; i < 10000; i += 100) {
    test(generate_1_case(i), "tree_search_report_1.txt", [](std::vector<std::string>&& values) {
      binary_tree tree;
      std::string to_find = values[values.size() % rand() - 1];
      for (std::string value: values) {
        tree.insert(std::move(value));
      }
      tree.find(to_find);
      return tree.get_search_comparisons();
    });
  }
  for (size_t i = 100; i < 10000; i += 100) {
    test(generate_2_case(i), "tree_search_report_2.txt", [](std::vector<std::string>&& values) {
      binary_tree tree;
      std::string to_find = values[values.size() % rand() - 1];
      for (std::string value: values) {
        tree.insert(std::move(value));
      }
      tree.find(to_find);
      return tree.get_search_comparisons();
    });
  }
  for (size_t i = 100; i < 10000; i += 100) {
    test(generate_3_case(i), "tree_search_report_3.txt", [](std::vector<std::string>&& values) {
      binary_tree tree;
      std::string to_find = values[values.size() % rand() - 1];
      for (std::string value: values) {
        tree.insert(std::move(value));
      }
      tree.find(to_find);
      return tree.get_search_comparisons();
    });
  }

  /*! Usuwanie. */
  for (size_t i = 100; i < 10000; i += 100) {
    test(generate_1_case(i), "tree_erase_report_1.txt", [](std::vector<std::string>&& values) {
      binary_tree tree;
      std::string to_erase = values[values.size() % rand() - 1];
      for (std::string value: values) {
        tree.insert(std::move(value));
      }
      tree.erase(to_erase);
      return tree.get_erase_comparisons();
    });
  }
  for (size_t i = 100; i < 10000; i += 100) {
    test(generate_2_case(i), "tree_erase_report_2.txt", [](std::vector<std::string>&& values) {
      binary_tree tree;
      std::string to_erase = values[values.size() % rand() - 1];
      for (std::string value: values) {
        tree.insert(std::move(value));
      }
      tree.erase(to_erase);
      return tree.get_erase_comparisons();
    });
  }
  for (size_t i = 100; i < 10000; i += 100) {
    test(generate_3_case(i), "tree_erase_report_3.txt", [](std::vector<std::string>&& values) {
      binary_tree tree;
      std::string to_erase = values[values.size() % rand() - 1];
      for (std::string value: values) {
        tree.insert(std::move(value));
      }
      tree.erase(to_erase);
      return tree.get_erase_comparisons();
    });
  }
}
