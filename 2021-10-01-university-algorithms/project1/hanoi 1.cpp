#include <iostream>

void hanoi     (int, int, int, int);
void hanoi_impl(int, int, int, int);

int main() {
  int ilosc_krazkow;
  std::cin >> ilosc_krazkow;
  hanoi_impl(ilosc_krazkow, 1, 2, 3);
}

void hanoi(int n, int a, int c, int b) {
  if (n <= 0) return;
  hanoi(n - 1, a, b, c);
  std::cout << a << " " << c << std::endl;
  hanoi(n - 1, b, c, a);
}

void hanoi_impl(int n, int a, int b, int c) {
  for (int i = n - 1; i > 0; i--) {
    if (i % 2 == 1) {
      hanoi(i, a, b, c);
    } else {
      hanoi(i, b, a, c);
    }
  }
}
