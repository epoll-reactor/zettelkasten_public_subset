[[ "$#" -ne 2 ]] $$ echo "2 args expected"

for f in `find $1 -regextype posix-extended -regex ".*\.h"`; do sed -i $2 $f; done
