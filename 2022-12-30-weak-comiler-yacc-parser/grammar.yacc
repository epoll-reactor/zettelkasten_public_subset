%{
#include "front_end/ast/ast_num.h"

#include <stdio.h>
#define YYSTYPE double
int yyerror(const char *msg);
extern int yylex();
extern char *yytext;
%}

%token TOK_BOOL
%token TOK_BREAK
%token TOK_CHAR
%token TOK_CONTINUE
%token TOK_DO
%token TOK_ELSE
%token TOK_FALSE
%token TOK_FLOAT
%token TOK_FOR
%token TOK_IF
%token TOK_INT
%token TOK_RETURN
%token TOK_STRUCT
%token TOK_TRUE
%token TOK_VOID
%token TOK_WHILE
%token TOK_CHAR_LITERAL
%token TOK_INTEGRAL_LITERAL
%token TOK_FLOATING_POINT_LITERAL
%token TOK_STRING_LITERAL
%token TOK_SYMBOL
%token TOK_ASSIGN
%token TOK_MUL_ASSIGN        
%token TOK_DIV_ASSIGN       
%token TOK_MOD_ASSIGN        
%token TOK_PLUS_ASSIGN      
%token TOK_MINUS_ASSIGN      
%token TOK_SHL_ASSIGN       
%token TOK_SHR_ASSIGN         
%token TOK_BIT_AND_ASSIGN    
%token TOK_BIT_OR_ASSIGN     
%token TOK_XOR_ASSIGN        
%token TOK_AND               
%token TOK_OR                 
%token TOK_XOR               
%token TOK_BIT_AND           
%token TOK_BIT_OR            
%token TOK_EQ                 
%token TOK_NEQ                
%token TOK_GT                
%token TOK_LT                
%token TOK_GE                 
%token TOK_LE                 
%token TOK_SHL                
%token TOK_SHR                
%token TOK_PLUS
%token TOK_MINUS             
%token TOK_STAR              
%token TOK_SLASH             
%token TOK_MOD               
%token TOK_INC                
%token TOK_DEC                
%token TOK_DOT               
%token TOK_COMMA            
%token TOK_SEMICOLON         
%token TOK_NOT               
%token TOK_OPEN_BOX_BRACKET  
%token TOK_CLOSE_BOX_BRACKET
%token TOK_OPEN_CURLY_BRACKET
%token TOK_CLOSE_CURLY_BRACKET
%token TOK_OPEN_PAREN     
%token TOK_CLOSE_PAREN

%left '*'

%start program

%%

program: expr TOK_SEMICOLON { printf("Result: %f\n", $1); }

expr: additive | multiplicative 

additive: multiplicative { $$ = $1; }
        | multiplicative TOK_PLUS additive { $$ = $1 + $3; printf("%f + %f\n", $1, $3); }
        | multiplicative TOK_MINUS additive { $$ = $1 - $3; printf("%f - %f\n", $1, $3); }

multiplicative: TOK_INTEGRAL_LITERAL { $$ = $1; }
              | TOK_INTEGRAL_LITERAL TOK_STAR multiplicative { $$ = $1 * $3; printf("%f * %f\n", $1, $3); }
              | TOK_INTEGRAL_LITERAL TOK_SLASH multiplicative { $$ = $1 / $3; printf("%f / %f\n", $1, $3); }

%%

int yyerror(char const *s) {
  printf("error: %s\n", s);
  return 0;
}

int main()
{
 return yyparse();
}

int yywrap()
{
  return 1;
}
