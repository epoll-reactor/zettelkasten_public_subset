#include <stdatomic.h>
#include <pthread.h>
#include <stdio.h>
#include <unistd.h>

static atomic_bool notify_stdio;
static atomic_int  notify_counter;

void *worker_stdio(void *arg)
{
    (void) arg;

    while (1) {
        atomic_store(&notify_stdio, 1);
        atomic_fetch_add(&notify_counter, 1);

        usleep(666);
    }

    return NULL;
}

void *worker_send(void *arg)
{
    (void) arg;

    while (1) {
        while (!atomic_load(&notify_stdio))
            ;

        printf("Sending %d...\n", atomic_load(&notify_counter));

        atomic_store(&notify_stdio, 0);

        usleep(666);
    }

    return NULL;
}

int main()
{
    pthread_t th1 = {0};
    pthread_t th2 = {0};

    pthread_create(&th1, NULL, worker_stdio, NULL);
    pthread_create(&th2, NULL, worker_send,  NULL);

    pthread_join(th1, NULL);
    pthread_join(th2, NULL);
}