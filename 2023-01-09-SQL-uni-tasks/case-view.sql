SELECT     * FROM OSOBY;
-- column 21 days after
SELECT     IMIE1, NAZWISKO, D_UR, D_UR + 21 as KURWA
FROM       OSOBY
WHERE      TO_CHAR(D_UR, 'YYYY') = '1978'
AND        (SUBSTR(NAZWISKO, 0, 1) = 'K' OR NAZWISKO LIKE '%A')
UNION ALL
SELECT     IMIE1, NAZWISKO, D_UR, D_UR + 21 as FUCK
FROM       OSOBY
WHERE      D_UR > TO_DATE('09/05/1978', 'DD/MM/YYYY')
;
COMMIT;
-- basic switch case
SELECT    IMIE1, IMIE2, NAZWISKO,
CASE      PLEC
          WHEN 'K' THEN 'FRAU'
          WHEN 'M' THEN 'MENSCH'
          ELSE          'ERROR'
END AS    GENDER
FROM OSOBY;
COMMIT;
-- custom condition in switch case
SELECT    IMIE1, IMIE2, NAZWISKO,
CASE
  WHEN PLEC = 'K' AND (SUBSTR(NAZWISKO, 0, 1) = 'K' OR NAZWISKO LIKE '%A')
    THEN 'FRAU'
  WHEN PLEC = 'M' AND (SUBSTR(NAZWISKO, 0, 1) = 'K' OR NAZWISKO LIKE '%A')
    THEN 'MENSCH'
  ELSE 'ERROR'
END AS    GENDER
FROM OSOBY;
COMMIT;
-- view. 12 * is multiplication by 12 (for one year)
CREATE OR REPLACE VIEW V_PENSJE
AS SELECT              ID_OS, 12 * PENSJA ANNUAL
FROM                   ZATRUDNIENIA
WHERE                  DO IS NULL
ORDER BY               2 DESC;

SELECT *
FROM   V_PENSJE;
COMMIT;
-- select from all stored views, USER_VIEWS is also view.
SELECT *
FROM   USER_VIEWS
WHERE  VIEW_NAME = 'V_PENSJE';