#include <fcntl.h>
#include <stdio.h>
#include <sys/stat.h>

int main(int argc, char **argv) {
	if (argc < 2) {
		printf("Usage: inode-info <filename>\n");
		return 1;
	}
	int fd = open(argv[1], O_RDONLY);
	if (fd < 0) {
		perror("open()");
		return 1;
	}
	struct stat f_stat;
	if (fstat(fd,&f_stat) < 0) {
		perror("fstat()");
		return 1;
	}
	printf(
		"ID of device containing file:  %lu\n"
		"inode number:                  %lu\n"
		"protection:                    %d\n"
		"number of hard links:          %ld\n"
		"user ID of owner:              %d\n"
		"group ID of owner:             %d\n"
		"device ID (if special file):   %ld\n"
		"total size, in bytes:          %ld\n"
		"blocksize for file system I/O: %ld\n"
		"512B blocks allocated:         %ld\n"
		"time of last access:           %ld\n"
		"time of last modification:     %ld\n"
		"time of last status change:    %ld\n",
		f_stat.st_dev,     f_stat.st_ino,    f_stat.st_mode,  f_stat.st_nlink,
		f_stat.st_uid,     f_stat.st_gid,    f_stat.st_rdev,  f_stat.st_size,
		f_stat.st_blksize, f_stat.st_blocks, f_stat.st_atime, f_stat.st_mtime,
		f_stat.st_ctime
	);
	
}
