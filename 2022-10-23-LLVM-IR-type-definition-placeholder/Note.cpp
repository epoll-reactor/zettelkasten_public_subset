// https://stackoverflow.com/questions/64631562/creating-a-class-definition-using-llvm-c-api
llvm::Type *ClassAST::codegen(IRgen *irgen){
  // create a struct type with all the data_members
  llvm::StructType *class_StructType = llvm::StructType::create(irgen->TheContext);
  class_StructType->setName(Proto->Name);
  std::vector<llvm::Type *> DataTypes;
  for(int i=0;i<data_members.size();i++){
    DataTypes.push_back(llvm::Type::getDoubleTy(irgen->TheContext)); // assume all data types are doubles for now
  }
  class_StructType->setBody(DataTypes);
  // add the type to the symbol table (How to do this?)
  // .. ????? ..
  // codegen the function members
  for (int i=0;i<function_members.size();i++){
    auto RetVal = function_members[i]->codegen(irgen);
    if(!RetVal){
      // Error reading body, remove function.
      LogErrorV(("Error generating code for "+Proto->Name+"."+function_members[i]->Proto->Name).c_str());
    }
  }
  return class_StructType;
}
