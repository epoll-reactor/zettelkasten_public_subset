#include "simd-dump.h"

#include <immintrin.h>
#include <stdio.h>

void dump_128_bit_reg_as_ints(const char *prefix, void *reg_ptr)
{
    int regdump[4];
    _mm_storeu_epi32(regdump, *(__m128i *) reg_ptr);

    printf("%s 128-bit: [", prefix);
    for (int i = 0; i < 4; ++i) {
        printf("%02x", regdump[i]);
        if (i < 3)
            printf(", ");
    }
    printf("]\n");
}

void dump_256_bit_reg_as_ints(const char *prefix, void *reg_ptr)
{
    int regdump[8];
    _mm256_storeu_epi32(regdump, *(__m256i *) reg_ptr);

    printf("%s 256-bit: [", prefix);
    for (int i = 0; i < 8; ++i) {
        printf("%02x", regdump[i]);
        if (i < 7)
            printf(", ");
    }
    printf("]\n");
}

void dump_512_bit_reg_as_ints(const char *prefix, void *reg_ptr)
{
    int regdump[16];
    _mm512_storeu_epi32(regdump, *(__m512i *) reg_ptr);

    printf("%s 512-bit: [", prefix);
    for (int i = 0; i < 16; ++i) {
        printf("%02x", regdump[i]);
        if (i < 15)
            printf(", ");
    }
    printf("]\n");
}