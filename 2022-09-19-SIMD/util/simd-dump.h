#ifndef UTIL_SIMD_DUMP_H
#define UTIL_SIMD_DUMP_H

void dump_128_bit_reg_as_ints(const char *prefix, void *reg_ptr);
void dump_256_bit_reg_as_ints(const char *prefix, void *reg_ptr);
void dump_512_bit_reg_as_ints(const char *prefix, void *reg_ptr);

#endif// UTIL_SIMD_DUMP_H