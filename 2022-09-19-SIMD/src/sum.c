#include "util/simd-dump.h"

#include <assert.h>

#include <immintrin.h>

int sum(const int *mem, size_t size)
{
    int sum = 0;

    for (size_t i = 0; i < size; ++i)
        sum += mem[i];

    return sum;
}

int simd_128_sum(const int *mem, size_t size)
{
    int sum = 0;
    size_t aligned_size = (size / 4) * 4;

    __m128i vk0 = _mm_set1_epi8(0);
    __m128i vk1 = _mm_set1_epi16(1);
    __m128i vsum = _mm_setzero_si128();

    for (size_t i = 0; i < aligned_size; i += 4) {
        __m128i input_reg = _mm_loadu_epi32(mem + i);
        __m128i vl = _mm_unpacklo_epi32(input_reg, vk0);
        __m128i vh = _mm_unpackhi_epi32(input_reg, vk0);
        vsum = _mm_add_epi32(vsum, _mm_madd_epi16(vl, vk1));
        vsum = _mm_add_epi32(vsum, _mm_madd_epi16(vh, vk1));

//        dump_128_bit_reg_as_ints("input_reg: ", &input_reg);
//        dump_128_bit_reg_as_ints("vl:        ", &vl);
//        dump_128_bit_reg_as_ints("vh:        ", &vh);
//        dump_128_bit_reg_as_ints("vsum:      ", &vsum);
    }

    vsum = _mm_add_epi32(vsum, _mm_srli_si128(vsum, 8));
    vsum = _mm_add_epi32(vsum, _mm_srli_si128(vsum, 4));
    sum = _mm_cvtsi128_si32(vsum);

    for (size_t i = aligned_size; i < size; ++i)
        sum += mem[i];

    return sum;
}

int main()
{
    int mem[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13};
    int size = sizeof(mem) / sizeof(mem[0]);

    assert(sum(mem, size) == simd_128_sum(mem, size));
}
