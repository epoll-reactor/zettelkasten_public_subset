#include <stdio.h>
#include <immintrin.h>

void dump_512_bit_reg_as_ints(__m512i reg) {
    int regdump[16];
    _mm512_storeu_si512((__m512i_u *)(regdump), reg);

    printf("512-bit register: [");
    for (int i = 0; i < 16; ++i) {
        printf("%02x", regdump[i]);
        if (i < 15) {
            putchar(',');
            putchar(' ');
        }
    }
    printf("]\n");
}

void dump_256_bit_reg_as_ints(__m256i reg) {
    int regdump[8];
    _mm256_storeu_si256((__m256i_u *)(regdump), reg);

    printf("256-bit register: [");
    for (int i = 0; i < 8; ++i) {
        printf("%02x", regdump[i]);
        if (i < 7) {
            putchar(',');
            putchar(' ');
        }
    }
    printf("]\n");
}

/// Copy at most size elements from src to dest if they are less than target.
int copy_if_less_do_work(const int *src, int *dest, int size, int target) {
    int times = 0;
    for (int i = 0; i < size; ++i)
        if (src[i] < target)
            dest[times++] = src[i];
    return times;
}

void  copy_if_less_test() {
    int src[24] = {
        0,  1,  2,  3,  4,  5,  6,  7,
        8,  9, 10, 11, 12, 13, 14, 15,
        16, 17, 18, 19, 20, 21, 22, 23
    };
    int dest[24];
    int copied = copy_if_less_do_work(src, dest, sizeof(src) / sizeof(int), 15);
    for (int i = 0; i < copied; ++i)
        printf("%d ", dest[i]);
    printf("\n");
}

int permutations[1 << 16][16] /* __attribute__((aligned(64))) */;

void compute_permutations() {
    for (int m = 0; m < 1 << 16; m++) {
        int k = 0;
        for (int i = 0; i < 16; i++)
            if (m >> i & 1)
                permutations[m][k++] = i;
    }
}

  /// Copy at most size elements from src to dest if they are less than target.
  int simd_copy_if_less_do_work(const int *src, int *dest, int size, int target) {
    int aligned_size = (size / 8) * 8;
    int times = 0;

    printf("Aligned size: %d\n", aligned_size);

    /// Без опции -mavx512fp16 -fexcess-precision=16 иногда падает с SIGILL.
    ///
    /// https://gcc.gnu.org/onlinedocs/gcc/Half-Precision.html
    /// On x86 targets with SSE2 enabled, without -mavx512fp16,
    /// all operations will be emulated by software emulation and
    /// the float instructions. The default behavior for FLT_EVAL_METHOD
    /// is to keep the intermediate result of the operation as 32-bit
    /// precision. This may lead to inconsistent behavior between
    /// software emulation and AVX512-FP16 instructions. Using
    /// -fexcess-precision=16 will force round back after each operation.
    // __m512i pred = _mm512_set1_epi32(target);

    __m256i pred = _mm256_set1_epi32(target);

    for (int i = 0; i < aligned_size; i += 8) {
        __m256i     x = _mm256_loadu_epi32(src + i);
        __mmask8 mask = _mm256_cmpgt_epi32_mask(pred, x);
        // Почему-то работает без падений только с unaligned permutations и
        // unaligned load.
        __m256i   idx = _mm256_loadu_epi32(permutations + mask);
        __m256i     y = _mm256_permutexvar_epi32(idx, x);
        // Тоже важно иметь указатель для unaligned памяти.
        _mm256_storeu_si256((__m256i_u *)(dest + times), y);
        
        times += _popcnt32(mask);
        // printf("Times = %d\n", times);

        printf(
            "Mask = %03d or [%d %d %d %d %d %d %d %d]\n",
            mask,
            (mask & (1 << 7)) ? 1 : 0,
            (mask & (1 << 6)) ? 1 : 0,
            (mask & (1 << 5)) ? 1 : 0,
            (mask & (1 << 4)) ? 1 : 0,
            (mask & (1 << 3)) ? 1 : 0,
            (mask & (1 << 2)) ? 1 : 0,
            (mask & (1 << 1)) ? 1 : 0,
            (mask & (1 << 0)) ? 1 : 0
        );
    }

    for (int i = aligned_size; i < size; ++i)
      if (src[i] < target)
        dest[times++] = src[i];

    return times;
  }

void simd_copy_if_less_test() {
    int src[24];
    for (int i = 1; i < 24; ++i)
        src[i] = rand() % (i * 1000);
    int dest[24];
    int copied = simd_copy_if_less_do_work(src, dest,  sizeof(src) / sizeof(int), 1000);
    for (int i = 0; i < copied; ++i)
        printf("%d ", dest[i]);
    printf("\n");
}

int main() {
    printf("Checking CPU extensions... ");
    int has_all_required_sets = 0;
    has_all_required_sets += __builtin_cpu_supports("sse") ? 1 : 0;
    has_all_required_sets += __builtin_cpu_supports("avx2") ? 1 : 0;
    has_all_required_sets += __builtin_cpu_supports("avx512f") ? 1 : 0;

    if (!has_all_required_sets) {
        printf("failed, missing SSE, AVX-2 or AVX-512\n");
        return 0;
    }
    printf("OK\n");

    compute_permutations();

    for (int i = 0; i < 100; ++i) {
        printf("Regular copy_if...\n");
        copy_if_less_test();
        printf("SIMD copy_if...\n");
        simd_copy_if_less_test();
    }

    return 0;
}
