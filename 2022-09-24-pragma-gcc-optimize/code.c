#ifdef _MSC_VER
#pragma optimize("", on)
#elif defined(__clang__)
#pragma clang optimize on
#elif defined(__GNUC__)
#pragma GCC optimize ("O3")
#endif

int main() {
    int sum = 0;
    for (int i = 0; i < 1 << 15; ++i) {
        for (int j = 0; j < 1 << 15; ++j) {
            sum += i;
            sum += j;
        }
    }
    return sum;
}

#ifdef _MSC_VER
#pragma optimize("", off)
#elif defined(__clang__)
#pragma clang optimize off
#elif defined(__GNUC__)
#pragma GCC optimize ("O0")
#endif
