#include <vector>
#include <list>
#include <forward_list>
#include <queue>
#include <deque>

template <class T>
concept DoubleEndedContainer = requires(T list) {
  typename T::value_type;
  typename T::pointer;
  typename T::const_pointer;
  typename T::allocator_type;
  not typename T::non_existent_iterator;
  { list.push_back(0) } -> std::convertible_to<void>;
  { list.push_front(0) } -> std::convertible_to<void>;
};

template <DoubleEndedContainer>
void f() {}

int main() {
  static_assert(DoubleEndedContainer<std::deque<int>>);
  static_assert(DoubleEndedContainer<std::list<int>>);
  static_assert(DoubleEndedContainer<std::deque<int>>);
//static_assert(DoubleEndedContainer<std::forward_list<int>>);
//static_assert(DoubleEndedContainer<std::vector<int>>);
//static_assert(DoubleEndedContainer<std::queue<int>>);

  f<std::deque<int>>();
  f<std::list<int>>();
  f<std::deque<int>>();
//f<std::forward_list<int>>();
//f<std::vector<int>>();
//f<std::queue<int>>();
}
