Не загружается ядро, говорит, не может запустить процесс init.

Причины:
  кривое обновление ядра или GRUB.

Решение
  sudo grub-install --target=i386-pc /dev/sda1
  grub-mkconfig -o /boot/grub/grub.cfg
  mkinitcpio -P
