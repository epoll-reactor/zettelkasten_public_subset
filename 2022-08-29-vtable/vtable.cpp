#include <iostream>
#include <cassert>

struct Polymorphic {
	virtual ~Polymorphic() = default;
	virtual void Function() = 0;
};

struct Implementation : Polymorphic {
	void Function() override {}
};

struct Freestanding {};

template <typename Target, typename Base>
Target *dyn_cast(Base *B) {
	static_assert(sizeof(Target) >= sizeof(Base), "");

	Target T;
	unsigned char *BaseVtablePtr
		= reinterpret_cast<unsigned char *>(B);
	unsigned char *TargetVtablePtr
		= reinterpret_cast<unsigned char *>(&T);

	int counter = 8;
	std::cout << "Base: ";
	while (counter-- >= 0) {
		std::cout << *BaseVtablePtr++;
	}
	counter = 8;
	std::cout << "Base: ";
	while (counter-- >= 0) {
		std::cout << *TargetVtablePtr++;
	}

	return new Target{};
}

int main() {
	Implementation Impl;
	Polymorphic &Poly = Impl;
	
	assert( dynamic_cast<Implementation *>(&Poly));
	assert(!dynamic_cast<Freestanding *>(&Poly));

	assert( dyn_cast<Implementation *>(&Poly));
	assert(!dyn_cast<Freestanding *>(&Poly));
}
