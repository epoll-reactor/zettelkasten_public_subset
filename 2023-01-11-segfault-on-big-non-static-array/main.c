#include "wm.h"
#include <stdio.h>
#include <string.h>
#include <unistd.h>

int main()
{
    wm_init();
    /// Segfaults if not static due to the stack overflow.
    /// If static, it creates another section for this data,
    /// not in the stack.
    static uint8_t blue_buf[3840 * 2160];
    for (uint64_t i = 0; i < 3840 * 2160 - 4; i += 4) {
        blue_buf[i    ] = 255; /// B
        blue_buf[i + 1] = 0;   /// G
        blue_buf[i + 2] = 0;   /// R
        blue_buf[i + 3] = 80;   /// Transparency
    }

    static uint8_t dark_blue_buf[3840 * 2160];
    for (uint64_t i = 0; i < 3840 * 2160 - 4; i += 4) {
        dark_blue_buf[i    ] = 200; /// B
        dark_blue_buf[i + 1] = 0;   /// G
        dark_blue_buf[i + 2] = 0;   /// R
        dark_blue_buf[i + 3] = 80;   /// Transparency
    }

    while (1) {
        wm_flush_buf(1000, 0, 1080, 60, blue_buf);
        wm_flush_buf(1000, 60, 1080, 1920, dark_blue_buf);
        usleep(10000);
    }
    wm_deinit();
}
