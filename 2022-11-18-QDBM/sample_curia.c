// Directories API
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <curia.h>

int main()
{
    const char key[]   = "key";
    const char record[] = "Free text";

    CURIA *curia;
    char *val;

    if(!(curia = cropen("book.db", CR_OWRITER | CR_OCREAT, /*Buckets*/-1, /*Division*/1))) {
        fprintf(stderr, "cropen: %s\n", dperrmsg(dpecode));
        return 1;
    }

    clock_t start = clock();

    for (int i = 0; i < 1000000; ++i)
        if(!crput(curia, key, -1, record, -1, /*This is important*/CR_DOVER))
            fprintf(stderr, "crput: %s\n", dperrmsg(dpecode));

    clock_t end = clock();
    printf("Elapsed %f seconds for 1'000'000 records\n", (float)(end - start) / CLOCKS_PER_SEC);

    if (!croptimize(curia, /*Bucket size*/100))
        fprintf(stderr, "Failed to optimize database\n");

    if((val = crget(curia, key, -1, 0, -1, NULL))) {
        printf("Got record `%s`\n", val);
        free(val);
    } else
        fprintf(stderr, "crget: %s\n", dperrmsg(dpecode));

    if(!crclose(curia))
        fprintf(stderr, "crclose: %s\n", dperrmsg(dpecode));

    return 0;
}
