#include <depot.h>
#include <cabin.h>
#include <odeum.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

#define DBNAME   "odeum_db"

static void print_results(ODEUM *odeum, ODPAIR *pairs, int num)
{
    printf("Num: %d\n", num);
    int i, j;
    ODDOC *doc;

    for (i = 0; i < num; ++i) {
        if (!(doc = odgetbyid(odeum, pairs[i].id)))
            continue;

        const char *title = oddocgetattr(doc, "title");
        if (title)
            printf("Title: %s\n", title);

        const char *author = oddocgetattr(doc, "author");
        if (author)
            printf("Title: %s\n", author);
        
        const CBLIST *words = oddocawords(doc);
        for(j = 0; j < cblistnum(words); j++) {
            const char *word = cblistval(words, j, NULL);
            printf("Word: %s\n", word);
        }
    }
    putchar('\n');
}

int main(int argc, char **argv)
{
    ODEUM *odeum;
    ODPAIR *pairs;
    ODDOC *doc;
    CBLIST *awords;
    const char *asis;
    char *normal;
    int i;

    if(!(odeum = odopen(DBNAME, OD_OWRITER | OD_OCREAT))) {
        fprintf(stderr, "odopen: %s\n", dperrmsg(dpecode));
        return 1;
    }

    {
        doc = oddocopen("http://www.foo.bar/baz.txt1");

        oddocaddattr(doc, "title", "Title1");
        oddocaddattr(doc, "author", "Author1");

        awords = odbreaktext("one two");

        for(i = 0; i < cblistnum(awords); i++) {
            asis = cblistval(awords, i, NULL);
            normal = odnormalizeword(asis);
            oddocaddword(doc, normal, asis);
            free(normal);
        }

        if(!odput(odeum, doc, -1, /*overwrite*/0))
            fprintf(stderr, "odput: %s\n", dperrmsg(dpecode));
    }
    
    {
        doc = oddocopen("http://www.foo.bar/baz.txt2");

        oddocaddattr(doc, "title", "Title2");
        oddocaddattr(doc, "author", "Author2");

        awords = odbreaktext("three four");

        for(i = 0; i < cblistnum(awords); i++) {
            asis = cblistval(awords, i, NULL);
            normal = odnormalizeword(asis);
            oddocaddword(doc, normal, asis);
            free(normal);
        }

        if(!odput(odeum, doc, -1, /*overwrite*/0))
            fprintf(stderr, "odput: %s\n", dperrmsg(dpecode));
    }

    cblistclose(awords);
    oddocclose(doc);
    odsync(odeum);

    int np;
    CBLIST errors;
    // Search query applied to text within document.
    if (pairs = odquery(odeum, "one|three", &np, &errors)) print_results(odeum, pairs, np);
    if (pairs = odquery(odeum, "(one&two)|(three&four)", &np, &errors)) print_results(odeum, pairs, np);
    if (pairs = odquery(odeum, "one three", &np, &errors)) print_results(odeum, pairs, np); // Implicit `&`.
    if (pairs = odquery(odeum, "one!(three!four)", &np, &errors)) print_results(odeum, pairs, np);

    if(!odclose(odeum)) {
        fprintf(stderr, "odclose: %s\n", dperrmsg(dpecode));
        return 1;
    }

    return 0;
}
