// Files API
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <depot.h>

static void get(DEPOT *depot, const char *key)
{
    char *record;
    if((record = dpget(depot, key, -1, 0, -1, NULL))) {
        printf("Got record `%s`\n", record);
        free(record);
    } else
        fprintf(stderr, "dpget: %s\n", dperrmsg(dpecode));
}

static void get_all(DEPOT *depot, void(*cb)(char *))
{
    if (!dpiterinit(depot)) {
        fprintf(stderr, "Failed to init iterator: %s\n", dperrmsg(dpecode));
        return;
    }

    char *key;
    while ((key = dpiternext(depot, NULL)))
        cb(key);
}

static void print_key(char *key)
{
    printf("Got key: %s\n", key);
}

int main()
{
    const char key[]   = "key";
    const char record[] = "Free text";

    DEPOT *depot;
    char *val;

    if(!(depot = dpopen("book.db", DP_OWRITER | DP_OCREAT, /*Buckets*/-1))) {
        fprintf(stderr, "dpopen: %s\n", dperrmsg(dpecode));
        return 1;
    }

    clock_t start = clock();

    for (int i = 0; i < 10; ++i) {
        char buf[10];
        sprintf(buf, "%s%d", key, i);
        if(!dpput(depot, buf, strlen(buf), record, -1, /*This is important*/DP_DOVER)) {
            fprintf(stderr, "dpput: %s\n", dperrmsg(dpecode));
            return -1;
        }
    }

    clock_t end = clock();
    printf("dpput():  elapsed %f seconds for 1'000'000 records\n", (float)(end - start) / CLOCKS_PER_SEC);

    if (!dpoptimize(depot, /*Bucket size*/100))
        fprintf(stderr, "Failed to optimize database\n");
    
    get(depot, "key0");
    get_all(depot, print_key);

    if(!dpclose(depot))
        fprintf(stderr, "dpclose: %s\n", dperrmsg(dpecode));

    return 0;
}
