#include <cabin.h>
#include <stdlib.h>
#include <stdio.h>

int main()
{
    CBDATUM *datum;
    CBLIST *list;
    CBMAP *map;
    char *buf1, *buf2;
    int i;

    /* open the datum handle */
    datum = cbdatumopen("123", -1);
    /* concatenate the data */
    cbdatumcat(datum, "abc", -1);
    /* print the datum */
    printf("%s\n", cbdatumptr(datum));
    /* close the datum handle */
    cbdatumclose(datum);

    /* open the list handle */
    list = cblistopen();
    /* add elements into the list */
    cblistpush(list, "apple", -1);
    cblistpush(list, "orange", -1);
    /* print all elements */
    for(i = 0; i < cblistnum(list); i++){
        printf("%s\n", cblistval(list, i, NULL));
    }
    /* close the list handle */
    cblistclose(list);

    /* open the map handle */
    map = cbmapopen();
    /* add records into the map */
    cbmapput(map, "dog", -1, "bowwow", -1, 1);
    cbmapput(map, "cat", -1, "meow", -1, 1);
    /* search for values and print them */
    printf("%s\n", cbmapget(map, "dog", -1, NULL));
    printf("%s\n", cbmapget(map, "cat", -1, NULL));
    /* close the map */
    cbmapclose(map);

    /* Base64 encoding */
    buf1 = cbbaseencode("I miss you.", -1);
    printf("%s\n", buf1);
    /* Base64 decoding */
    buf2 = cbbasedecode(buf1, NULL);
    printf("%s\n", buf2);
    /* release the resources */
    free(buf2);
    free(buf1);

    /* register a plain pointer to the global garbage collector */
    buf1 = cbmemdup("Take it easy.", -1);
    cbglobalgc(buf1, free);
    /* the pointer is available but you don't have to release it */
    printf("%s\n", buf1);
    
    /* register a list to the global garbage collector */
    list = cblistopen();
    cbglobalgc(list, (void (*)(void *))cblistclose);
    /* the handle is available but you don't have to close it */
    cblistpush(list, "Don't hesitate.", -1);
    for(i = 0; i < cblistnum(list); i++){
        printf("%s\n", cblistval(list, i, NULL));    
    }

    return 0;
}
