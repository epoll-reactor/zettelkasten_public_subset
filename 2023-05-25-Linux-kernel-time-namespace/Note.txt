  ┌──────────────────────────────────── TIME namespace ────────────────────────────────────┐
  │ CONFIG_TIME_NS:                                                                        │
  │                                                                                        │
  │ In this namespace boottime and monotonic clocks can be set.                            │
  │ The time will keep going with the same pace.                                           │
  │                                                                                        │
  │ Symbol: TIME_NS [=n]                                                                   │
  │ Type  : bool                                                                           │
  │ Defined at init/Kconfig:1220                                                           │
  │   Prompt: TIME namespace                                                               │
  │   Depends on: NAMESPACES [=y] && GENERIC_VDSO_TIME_NS [=y]                             │
  │   Location:                                                                            │
  │     -> General setup                                                                   │
  │       -> Namespaces support (NAMESPACES [=y])                                          │
  │         -> TIME namespace (TIME_NS [=n])                                               │
  │                                                                                        │
  │                                                                                        │
   ────────────────────────────────────────────────────────────────────────────────────────

The time namespace allows processes to see different system times in a way similar
to the UTS namespace. It was proposed in 2018 and landed on Linux 5.6, which was
released in March 2020.
