#include <stdlib.h>
#include <stdio.h>

enum
{
    AST_NUMBER,
    AST_BINARY,
    AST_COMPOUND
};

typedef struct ast_node_t ast_node_t;

typedef struct {
    int val;
} ast_number_t;

typedef struct {
    char op;
    ast_node_t *lhs;
    ast_node_t *rhs;
} ast_binary_t;

typedef struct {
    int size;
    ast_node_t **stmts;
} ast_compound_t;

typedef struct ast_node_t {
    int type;
    void *ast;
} ast_node_t;

ast_node_t *ast_node_init(int type, void *ast)
{
    ast_node_t *node = malloc(sizeof(ast_node_t));
    node->type = type;
    node->ast = ast;
    return node;
}

ast_node_t *ast_number_init(int val)
{
    ast_number_t *ast = malloc(sizeof(ast_number_t));
    ast->val = val;
    return ast_node_init(AST_NUMBER, ast);
}

ast_node_t *ast_binary_init(char op, void *lhs, void *rhs)
{
    ast_binary_t *ast = malloc(sizeof(ast_binary_t));
    ast->op = op;
    ast->lhs = lhs;
    ast->rhs = rhs;
    return ast_node_init(AST_BINARY, ast);
}

ast_node_t *ast_compound_init(int size, ast_node_t **stmts)
{
    ast_compound_t *ast = malloc(sizeof(ast_compound_t));
    ast->size = size;
    ast->stmts = stmts;
    return ast_node_init(AST_COMPOUND, ast);
}

// @{
// Traverse functions used to print AST.
static int ast_indent = 0;

void print_indent()
{
    for (int i = 0; i < ast_indent; ++i)
        putchar(i % 2 == 0 ? '|' : ' ');
}

void ast_print(ast_node_t *ast);

void ast_print_number(ast_number_t *ast)
{
    printf("AST number `%d`\n", ast->val);
}

void ast_print_binary(ast_binary_t *ast)
{
    printf("AST binary `%c`\n", ast->op);

    ast_indent += 2;

    print_indent();
    ast_print(ast->lhs);

    print_indent();
    ast_print(ast->rhs);

    ast_indent -= 2;
}

void ast_print_compound(ast_compound_t *ast)
{
    printf("AST compound of size %d\n", ast->size);

    ast_indent += 2;

    for (int i = 0; i < ast->size; ++i) {
        print_indent();        
        ast_print(ast->stmts[i]);
    }

    ast_indent -= 2;
}

void ast_print(ast_node_t *ast)
{
    switch (ast->type) {
    case AST_NUMBER:
        ast_print_number(ast->ast);
        break;
    case AST_BINARY:
        ast_print_binary(ast->ast);
        break;
    case AST_COMPOUND:
        ast_print_compound(ast->ast);
        break;
    }
}
// @}

// @{
// Traverse functions used to cleanup AST.
void ast_cleanup(ast_node_t *ast);

void ast_cleanup_number(ast_number_t *ast)
{
    free(ast);
}

void ast_cleanup_binary(ast_binary_t *ast)
{
    ast_cleanup(ast->lhs);
    ast_cleanup(ast->rhs);
    free(ast);
}

void ast_cleanup_compound(ast_compound_t *ast)
{
    for (int i = 0; i < ast->size; ++i)
        ast_cleanup(ast->stmts[i]);

    free(ast->stmts);
    free(ast);
}

void ast_cleanup(ast_node_t *ast)
{
    switch (ast->type) {
    case AST_NUMBER:
        ast_cleanup_number(ast->ast);
        break;
    case AST_BINARY:
        ast_cleanup_binary(ast->ast);
        break;
    case AST_COMPOUND:
        ast_cleanup_compound(ast->ast);
        break;
    }
    free(ast);
}
// @}

int main()
{
    ast_node_t **stmts = malloc(sizeof(ast_node_t *) * 2);

    stmts[0] =
        ast_binary_init(
            '+',
            ast_number_init(1),
            ast_binary_init(
                '*',
                ast_number_init(2),
                ast_number_init(3)
            )
        );

    stmts[1] =
        ast_binary_init(
            '-',
            ast_number_init(9),
            ast_binary_init(
                '/',
                ast_binary_init(
                    '%',
                    ast_number_init(8),
                    ast_number_init(7)
                ),
                ast_number_init(6)
            )
        );

    ast_node_t *compound = ast_compound_init(2, stmts);
    ast_print(compound);
    ast_cleanup(compound);
}
