#include <errno.h>
#include <fcntl.h>
#include <malloc.h>
#include <signal.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

/**
 * Важно: в коде GCC для выхода из функции используется
 * 0x000000000000113c <+35>:    5d      pop    %rbp
 * 0x000000000000113d <+36>:    c3      ret
 * , но мне нужно
 * 0x000000000000113c <+35>:    c9      leave
 * 0x000000000000113d <+36>:    c3      ret
 * , без этого всё идёт по пизде.
 *
 * uint32_t f()
 *   {
 *     int result = 0;
 *     for (int i = 0; i < 100; ++i)
 *       {
 *         ++result;
 *       }
 *     return result;
 *   }
 */
uint8_t code [] =
  {
    0x55,                                       /** 0x0000000000001119 <+ 0>: push   %rbp */
    0x48, 0x89, 0xe5,                           /** 0x000000000000111a <+ 1>: mov    %rsp,%rbp */
    0xc7, 0x45, 0xf8, 0x00, 0x00, 0x00, 0x00,   /** 0x000000000000111d <+ 4>: movl   $0x0,-0x8(%rbp) */
    0xc7, 0x45, 0xfc, 0x00, 0x00, 0x00, 0x00,   /** 0x0000000000001124 <+11>: movl   $0x0,-0x4(%rbp) */
    0xeb, 0x06,                                 /** 0x000000000000112b <+18>: jmp    0x1133 <f+26> */
    0xff, 0x45, 0xf8,                           /** 0x000000000000112d <+20>: incl   -0x8(%rbp) */
    0xff, 0x45, 0xfc,                           /** 0x0000000000001130 <+23>: incl   -0x4(%rbp) */
    0x83, 0x7d, 0xfc, 0x63,                     /** 0x0000000000001133 <+26>: cmpl   $0x63,-0x4(%rbp) */
    0x7e, 0xf4,                                 /** 0x0000000000001137 <+30>: jle    0x112d <f+20> */
    0x8b, 0x45, 0xf8,                           /** 0x0000000000001139 <+32>: mov    -0x8(%rbp),%eax */
    0xc9,                                       /** 0x000000000000113c <+35>: leave  */
    0xc3                                        /** 0x000000000000113d <+36>: ret    */
  };

typedef uint32_t (*code_sign_t) ();

void exec_stack_x ()
  {
    uint8_t stack [4096];
    memcpy(stack, code, sizeof (code));

    size_t pagesize = sysconf (_SC_PAGESIZE);
    uintptr_t start = (uintptr_t) &stack;
    uintptr_t end = start + sizeof (stack);
    ///  start:      101110011101001001100111010000
    ///  pagesize: 00000000000000000001000000000000
    /// -pagesize: 11111111111111111111000000000000
    /// pagestart:   101110011101001001000000000000
    uintptr_t pagestart = start & -pagesize;

    if ( -1 == mprotect ( (void *) pagestart, end - pagestart, PROT_EXEC | PROT_READ | PROT_WRITE ) )
      {
        perror ("mprotect(RWX)");
        exit (1);
      }

    uint32_t exit_code = ( (code_sign_t)(stack) ) ();
    printf ("stack_x(): exited with %u\n", exit_code);
    printf ("result == 100? %d\n", 100 == exit_code);
  }

void on_sigsegv (int /*sig*/, siginfo_t *si, void */*unused*/)
  {
    printf ("# SIGSEGV at %p.\n", si->si_addr);
    exit (-1);
  }

void install_sig_handlers ()
  {
    struct sigaction sa;
    sa.sa_flags = SA_SIGINFO;
    sigemptyset(&sa.sa_mask);
    sa.sa_sigaction = on_sigsegv;
    if (sigaction(SIGSEGV, &sa, NULL) == -1)
      {
        perror("sigaction");
      }
  }

int main()
  {
    install_sig_handlers();
    exec_stack_x();
  }
