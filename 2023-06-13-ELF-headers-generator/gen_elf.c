#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>

int main()
{
    int fd = open("generated.o", O_CREAT | O_TRUNC | O_RDWR, 0666);
    if (fd < 0) {
        perror("open()");
        return 1;
    }

    if (ftruncate(fd, 0x401500) < 0) {
        perror("ftruncate()");
        return 1;
    }

    char *map =  (char *) mmap(NULL, 0x401500, PROT_WRITE | PROT_READ, MAP_SHARED, fd, 0);
    if ((void *) map == MAP_FAILED) {
        perror("mmap()");
        return 1;
    }

    /// ELF header
    strcpy(&map[0x00], "\x7f");
    strcpy(&map[0x01], "ELF");
    /// 0x1 for 32 bit,
    /// 0x2 for 64 bit
    strcpy(&map[0x04], "\x02");
    /// 0x1 for little endian,
    /// 0x2 for big endian
    strcpy(&map[0x05], "\x01");
    /// ELF version. Always 1.
    strcpy(&map[0x06], "\x01");
    /// System ABI. I set for System V.
    strcpy(&map[0x07], "\x00");
    /// ABI version.
    strcpy(&map[0x08], "\x00");
    /// Padding byte. Unused.
    strcpy(&map[0x09], "\x00");
    /// Object file type. 2 is executable.
    strcpy(&map[0x10], "\x02");
    /// Instruction set architecture. 3E is AMD x86_64.
    strcpy(&map[0x12], "\x3E");
    /// Another byte for ELF version...
    strcpy(&map[0x14], "\x01");
    /// Address of program entry point.
    /// Little endian for some reason.
    memcpy(&map[0x18], "\x00\x10\x40", 4);

    /// !!!
    /// All things below are hard-coded.
    /// !!!
    
    /// Program header table start. It follows
    /// This header immediatly.
    strcpy(&map[0x20], "\x40\x00");
    /// Start of section header table.
    /// Hardcoded for now 4192 (0x1060).
    strcpy(&map[0x28], "\x60\x10");
    /// Some flags. Idk.
    strcpy(&map[0x30], "\x00");
    /// Size of the header. Normally is 64 bytes.
    strcpy(&map[0x34], "\x40");
    /// Program header table size. Idk. 64.
    strcpy(&map[0x36], "\x38");
    /// Number of entries in program header table.
    strcpy(&map[0x38], "\x04");
    /// Section header table size. Idk.
    strcpy(&map[0x3A], "\x40");
    /// Number of entries in section header table.
    strcpy(&map[0x3C], "\x05");
    /// Index of the section header table entry that contains the section names. 
    strcpy(&map[0x3E], "\x04");
    /// End of header. Actually size.
    strcpy(&map[0x40], "\x40");

    /// Start of program header
    ///
    /// Program table is unused
    // strcpy(&map[0x1000], "\xc3");

    char code[] = {
        0x48, 0xc7, 0xc0, 0x3c, 0x00, 0x00, 0x00, // mov $0x3c, %rax
        0x48, 0x31, 0xff,                         // xor %rdi, %rdi
        0x0f, 0x05                                // syscall
    };

    memcpy(&map[0x401000], &code[0], sizeof (code));

    if (munmap(map, 0x401500) < 0) {
        perror("munmap()");
        return 1;
    }

    if (close(fd) < 0) {
        perror("close()");
        return 1;
    }
}