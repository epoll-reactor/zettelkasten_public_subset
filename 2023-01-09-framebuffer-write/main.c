#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdbool.h>
#include <linux/fb.h>
#include <sys/mman.h>
#include <sys/ioctl.h>

static inline bool is_in_area(int x, int y, int off_x, int off_y, int siz_x, int siz_y)
{
    return (x > off_x && x < siz_x - off_x) && (y > off_y && y < siz_y - off_y);
}

int main()
{
    struct fb_var_screeninfo vinfo;
    struct fb_fix_screeninfo finfo;
    long int screensize = 0;
    char *fbp = 0;
    int x = 0, y = 0;
    long int location = 0;

    int fbfd = open("/dev/fb0", O_RDWR);
    if (fbfd == -1) {
        perror("open()");
        return -1;
    }

    if (ioctl(fbfd, FBIOGET_FSCREENINFO, &finfo)) {
        printf("Error reading fixed information.\n");
        return -2;
    }

    if (ioctl(fbfd, FBIOGET_VSCREENINFO, &vinfo)) {
        printf("Error reading variable information.\n");
        return -3;
    }

    printf("%dx%d, %dbpp\n", vinfo.xres, vinfo.yres, vinfo.bits_per_pixel);

    // Figure out the size of the screen in bytes
    screensize = vinfo.xres * vinfo.yres * vinfo.bits_per_pixel / 8;

    // Map the device to memory
    fbp = (char *)mmap(0, screensize, PROT_READ | PROT_WRITE, MAP_SHARED, fbfd, 0);
    if (fbp == NULL) {
        printf("Error: failed to map framebuffer device to memory.\n");
        return -4;
    }
    printf("The framebuffer device was mapped to memory successfully.\n");

    while (1) {
        // Figure out where in memory to put the pixel
        for ( y = 0; y < vinfo.yres; y++ ) {
            for ( x = 0; x < vinfo.xres; x++ ) {

                location = (x+vinfo.xoffset) * (vinfo.bits_per_pixel/8) + (y+vinfo.yoffset) * finfo.line_length;

                int off_x = 0;
                int off_y = 0;
                int siz_x = 1080;
                int siz_y = 50;

                if (is_in_area(x, y, off_x, off_y, siz_x, siz_y)) {
                // if ( vinfo.bits_per_pixel == 32 ) {
                    *(fbp + location) = 100;        // Some blue
                    *(fbp + location + 1) = 120;     // A little green
                    *(fbp + location + 2) = 200;    // A lot of red
                    *(fbp + location + 3) = 50;      // No transparency
                }

                off_x = 0;
                off_y = 50;
                siz_x = 1080;
                siz_y = 1800;

                if (is_in_area(x, y, off_x, off_y, siz_x, siz_y)) {
                // if ( vinfo.bits_per_pixel == 32 ) {
                    *(fbp + location) = 100;        // Some blue
                    *(fbp + location + 1) = 100;     // A little green
                    *(fbp + location + 2) = 100;    // A lot of red
                    *(fbp + location + 3) = 50;      // No transparency
                }
            }
        }
        usleep(1000);
    }
    munmap(fbp, screensize);
    close(fbfd);
    return 0;
}
